<?php

namespace App\Utils;


class NumberUtils {

    public static function toStringRupiah($number) {
        return "Rp. ".number_format($number,2,',','.');
    }
}