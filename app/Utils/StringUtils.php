<?php

namespace App\Utils;

use Carbon\Carbon;


class StringUtils {

    public static function toLocalDateString($stringDate) {
        return Carbon::parse($stringDate)->locale('id')->isoFormat('dddd, D MMMM Y');
    }
}