<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\User;

class UserRole extends Model {

    
    protected $table =  'user_role';

    public function user() {
        return $this->hasMany(User::class);
    }
}