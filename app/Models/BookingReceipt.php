<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Booking;
use Illuminate\Support\Facades\Auth;

class BookingReceipt extends Model {
    
    protected $table = 'booking_receipts';
    
    public function booking() {
        return $this->belongsTo(Booking::class);   
    }

}