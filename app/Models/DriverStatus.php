<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Driver;

class DriverStatus extends Model {
    
    protected $table = 'driver_status';
    
    public function driver() {
        return $this->belongsTo(Driver::class);   
    }

}