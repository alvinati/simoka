<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Booking;

class BookingStatus extends Model {
    
    protected $table = 'booking_status';
    
    public function booking() {
        return $this->hasMany(Booking::class);   
    }

}