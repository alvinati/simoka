<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\EmployeeDivision;
use App\Models\EmployeeTitle;
use App\Models\User;
use App\Models\UserRequest;
use App\Models\Booking;

class Employee extends Model {

    use SoftDeletes;

    protected $table =  'employees';

    public function division() {
        return $this->belongsTo(EmployeeDivision::class);
    }

    public function title() {
        return $this->belongsTo(EmployeeTitle::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function driver() {
        return $this->hasOne(Driver::class);
    }

    public function request() {
        return $this->hasMany(UserRequest::class);
    }

    public function booking() {
        return $this->hasManyThrough(Booking::class, UserRequest::class, 'employee_id', 'request_id', 'id', 'id');
    }
}