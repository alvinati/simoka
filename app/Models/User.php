<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

use App\Models\Employee;
use App\Models\UserRole;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'login_first_time' => 'datetime',
    ];

    public function employee() {
        $this->hasOne(Employee::class);
    }

    public function role() {
        $this->belongsTo(UserRole::class);
    }


    public function hasRole($role) {
       switch($role) {
           case 'admin':
            return Auth::user()->role_id == 1;
           case 'reviewer':
            return Auth::user()->role_id == 2;
           case 'requestor':
            return Auth::user()->role_id == 3;
           default: 
            return false;
       }
    }
}
