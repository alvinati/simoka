<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Model\Employee;

class EmployeeTitle extends Model {

    use SoftDeletes;
    
    protected $table =  'employee_titles';

    public function employee() {
        return $this->hasMany(Employee::class);
    }
}