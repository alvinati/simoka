<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Notifications\ResetPassword;
use WeStacks\TeleBot\TeleBot;
use App\Models\TelegramSession;

class PasswordToken extends Model {

    protected $table = 'password_resets';

    
    public function add($email, $token)
    {
        $pass = new PasswordToken;
        $pass->email = $email;
        $pass->token = $token;
        $pass->save();
        return $pass;
    }

    public function sendResetLink($usernameTele, $token) {

        $session = TelegramSession::where('username', $usernameTele)->first();
        if($session == null) {
            return 'Anda belum verifikasi username Telegram';
        }else if($session->chat_id == null ) {
            return 'Data akun Anda tidak lengkap';
        }
        
        $url = env('APP_URL').'ganti-password/'.$token;
        $message = "Silahkan klik button link dibawah untuk ganti password";
        
        $teleChat = (object) array(
            'telegram_chat_id' => $session->chat_id,
            'url_text' => 'Link Ganti Password',
            'message' => $message,
            'url' => $url
        );

        $result = $this->sendLinkNotification($teleChat);
        
        return 'success';
    }

    private function sendLinkNotification($teleChat) {
        $teleChat->url_text = $teleChat->url_text == NULL ? 'Link' : $teleChat->url_text;
        $bot = new TeleBot(env('TELEGRAM_BOT_TOKEN'));
        $keyboard = [
            'inline_keyboard' => [
                [
                    ['text' => $teleChat->url_text, 'url' => $teleChat->url]
                ]
            ]
        ];

        return $bot->sendMessage([
                    'chat_id' => $teleChat->telegram_chat_id,
                    'text'    => $teleChat->message,
                    'reply_markup' => $keyboard
                    
                ]);
    }
}