<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Employee;
use App\Models\Vehicle;
use App\Models\DriverStatus;
use App\Models\UserRequest;

class Driver extends Model {
    use SoftDeletes;
    
    protected $table = 'drivers';

    public function car() {
        return $this->belongsTo(Vehicle::class);   
    }

    public function employee() {
        return $this->belongsTo(Employee::class);
    }

    public function status() {
        return $this->belongsTo(DriverStatus::class);
    }

    public function availability() {
        return $this->belongsTo(DriverStatus::class);
    }

    public function request() {
        return $this->hasMany(UserRequest::class);
    }
}