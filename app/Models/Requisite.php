<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\UserRequest;

class Requisite extends Model {

    protected $table = 'request_requisite';

    public function request() {
        return $this->hasMany(UserRequest::class);
    }

}