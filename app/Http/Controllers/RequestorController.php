<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


use App\Http\Requests\EmployeeRequest;

use App\Models\User;
use App\Models\UserRequest;
use App\Models\Requisite;
use App\Models\Driver;
use App\Models\Employee;
use App\Models\EmployeeDivision;
use App\Models\EmployeeTitle;
use App\Models\Booking;
use App\Models\Vehicle;
use App\Models\BookingReceipt;
use App\Models\TelegramSession;

use App\Utils\StringUtils;
use App\Utils\NumberUtils;


class RequestorController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $user = Auth::user();
        $now = Carbon::now();
        $date = $now->locale('id')->isoFormat('LLLL');

        $employee = Employee::select('id')->where('user_id', $user->id)->first();
        $requests = UserRequest::with(['status', 'employee'])
            ->where('employee_id', $employee->id)
            ->whereDate('created_at', Carbon::today())
            ->simplePaginate(5);

        return view('layouts.requestor.dashboard',
             ['user_fullname' => $user->name, 'user_role' => $user->role_id, 'currentDate'=>$date, 'requests' => $requests]);
    }

    public function requestDetail($requestId) {
        $user = Auth::user();
        $requestData = UserRequest::where('id', $requestId)->first();

        if($requestData->driver_id == NULL){
            $requestData = UserRequest::with(['employee', 'requisite', 'status'])
                ->where('id', $requestId)->first();
        } else {
            $driverId = $requestData->driver_id;
            $driver = Driver::where('id', $driverId)->first();
            $driverName = Employee::where('id', $driver->employee_id)->first()->name;

            $requestData = UserRequest::with(['employee', 'requisite', 'status'])
                ->where('id', $requestId)->first();

            $requestData->{"driver_name"} = $driverName;
        };
       
        $requestData->travel_date =  StringUtils::toLocalDateString($requestData->travel_date);
        
        $booking = Booking::select('book_number', 'id')->where('request_id', $requestId)->first();
        if($booking != NULL) {
            $requestData->{'booking_number'} = $booking->book_number;
            $requestData->{'booking_id'} = $booking->id;
        }

        return view('layouts.requestor.requestdetail', ['user_fullname' => $user->name, 'user_role' => $user->role_id,
            'data' => $requestData
        ]);
    }

    public function requestForm() {
        $user = Auth::user();

        $requisites = Requisite::all();

        return view('layouts.requestor.formrequest', ['user_fullname' => $user->name, 'user_role' => $user->role_id, 'requisites' => $requisites]);
    }

    public function submitForm(Request $request) {
        $user = Auth::user();

        $validator = Validator::make($request->all(), [
            "travel_date" => "required|date|after:yesterday",
            "travel_time" => "required",
            "destination" => "required|string",
            "requisite" => "required|min:1",
            "passenger" => "required|int|min:1|max:5"
        ], $mesaages = [
            "travel_date.required" => "Tanggal berangkat harus diisi",
            "travel_time.required" => "Jam berangkat harus diisi",
            "travel_date.after" => "Tanggal berangkat minimal hari ini",
            "destination.required" => "Tujuan harus di isi",
            "requisite.min" => "Silahkan pilih keperluan yang tersedia",
            "passenger.required" => "Jumlah penumpang harus diisi",
            "passenger.min" => "Jumlah penumpang minimal 1 orang",
            "passenger.max" => "Jumlah penumpang maksimal 5 orang"
        ]);

        if($validator->fails()) {
            return redirect('form-pengajuan')
                    ->withErrors($validator)
                    ->withInput();
        }

        if(strtoTime("$request->travel_date $request->travel_time") < time()) {
            return redirect('form-pengajuan')
                    ->withErrors("Tidak bisa mengirim pengajuan untuk keberangkatan dengan waktu kurang dari sekarang")
                    ->withInput();
        }

        $employee = Employee::select('id', 'name')->where('user_id', $user->id)->first();
        $newRequest = new UserRequest;
        $newRequest->employee_id = $employee->id;
        $newRequest->status_id = 1;
        $newRequest->requisite_id = $request->requisite;
        $newRequest->total_passenger = $request->passenger;
        $newRequest->created_by = $user->id;
        $newRequest->updated_by = $user->id;
        $number = $this->createNumber($employee);
        $newRequest->request_number = $number;
        $newRequest->travel_date = $request->travel_date;
        $newRequest->travel_time = $request->travel_time;
        $newRequest->destination = $request->destination;
        $newRequest->save();

        $this->sendNotifToAdmin($newRequest);
        return redirect('form-pengajuan')->with('status', 'Pengajuan berhasil dikirim, mohon tunggu informasi selanjutnya melalui pesan telegram');

    }

    public function showRequestLists() {
        $user = Auth::user();
        $this->updateRequestsData();

        $employee = Employee::select('id')->where('user_id', $user->id)->first();
        $requests = UserRequest::with(['employee', 'requisite', 'status'])
                        ->where('employee_id', $employee->id)
                        ->orderBy('created_at', 'desc')
                        ->simplePaginate(5);

        foreach($requests as $r) {
            
            if($r->status->id == 1 && strtoTime($r->travel_date) < time()) { // if waiting and outdated
                $r->status_id = 5; //5 means outdated
                $r->save();
            }

            $r->travel_date = StringUtils::toLocalDateString($r->travel_date);
            
            if($r->driver_id != NULL) {  
                $driver = Driver::where('id',$r->driver_id)->first();
                $employee = Employee::where('id',$driver->employee_id)->first();
                $r->{'driver_name'} = $employee->name;
            }
        }

        return view('layouts.requestor.requesthistory', ['user_fullname' => $user->name, 'user_role' => $user->role_id,
                                        'requests' => $requests]);
    }

    public function bookingDetail($id) {
        $user = Auth::user();

        $data = Booking::with(['request', 'status'])->where('id', $id)->first();
        $driver = Driver::select('employee_id', 'car_id')->where('id', $data->request->driver_id)->first();
        $requisite = Requisite::select('requisite')->where('id', $data->request->requisite_id)->first();
        $car = Vehicle::select('plat_number', 'km')->where('id', $driver->car_id)->first();
        $driverName = Employee::select('name')->where('id', $driver->employee_id)->first();
        $requestor = Employee::select('name')->where('id', $data->request->employee_id)->first();
        
        $placeHolder = Storage::url('add_image.png');

        $data->request->travel_date = StringUtils::toLocalDateString($data->request->travel_date);
    
        $data->{'car'} = $car->plat_number;
        $data->{'km'} = number_format($car->km,2,',','.');
        $data->{'requisite'} = $requisite->requisite;
        $data->{'driver_name'} = $driverName->name;
        $data->{'requestor'} = $requestor->name;
        $data->{'place_holder'} = $placeHolder;

        if($data->status->id == 2) {
            $fuel_receipts = BookingReceipt::select('image_path')
                                ->where('booking_id', $data->id)
                                ->where('type', 'FUEL')
                                ->get();

            if(count($fuel_receipts) > 0)
                $data->{'fuel_receipt_paths'} = $fuel_receipts;
            
            $data->{'fuel_cost_string'} = NumberUtils::toStringRupiah($data->fuel_cost);
            
            if($data->toll_cost > 0) {
                $toll_receipts = BookingReceipt::select('image_path')
                                    ->where('booking_id', $data->id)
                                    ->where('type', 'TOLL')
                                    ->get();
                                    
                if(count($toll_receipts) > 0) {
                    $data->{'toll_receipt_paths'} = $toll_receipts;
                }

                $data->{'toll_cost_string'} = NumberUtils::toStringRupiah($data->toll_cost);
            }

            $data->{'total_cost_string'} = NumberUtils::toStringRupiah($data->total_cost);
        }

        return view('layouts.requestor.bookingdetail', ['user_fullname' => $user->name, 'user_role' => $user->role_id, 
                        'data' => $data]);
    }

    public function profile() {
        $user = Auth::user();

        $employee = Employee::with(['division', 'title'])->where('user_id', $user->id)->first();
        $user->{'employee'} = $employee;
        $divisions = EmployeeDivision::all();
        $titles = EmployeeTitle::all();

        return view('layouts.requestor.profile', ['user_fullname' => $user->name, 'user_role' => $user->role_id,
                    'user' => $user, 'divisions' => $divisions, 'titles' => $titles]);
    }

    public function updateMyProfile(EmployeeRequest $request) {
        $currentUserId = Auth::user()->id;
        $validated = $request->validated();
        $employee = Employee::where('user_id', $currentUserId)->first();
        $redirect = redirect("/myprofile")->with('status', 'Perubahan berhasil!');

       if($request->email != $employee->email) {
            $additionalValidator = Validator::make($request->all(), [
                'email' => 'unique:employees|unique:users'
            ], $messages = [
                'email.unique' => 'Karyawan / User dengan email yang diberikan sudah terdaftar'
            ]);
        
            if ($additionalValidator->fails()) {
                return redirect('myprofile')
                        ->withErrors($additionalValidator)
                        ->withInput();
            }
       }

       if($request->nik != $employee->nik) {
            $additionalValidator = Validator::make($request->all(), [
                'nik' => 'unique:employees'
            ], $messages = [
                'nik.unique' => 'Karyawan dengan NIK yang diberikan sudah terdaftar'
            ]);
            if ($additionalValidator->fails()) {
                return redirect('myprofile')
                            ->withErrors($additionalValidator)
                            ->withInput();
            }
        }
        
        $driverTitleId = EmployeeTitle::where('name', 'Driver')->first()->id;
        if($validated['title'] == EmployeeTitle::where('name', 'Driver')->first()->id) { //if current request title is driver
            
            if($validated['title'] != $employee->title_id) {//  if employee title is changed
                if(Driver::withTrashed()->where('employee_id', $employee->id)->first() == NULL) { //and if employee id is not exist as drivers table before
                    $driver = new Driver;
                    $driver->employee_id = $employee->id;
                    $driver->status_id = 1;
                    $driver->created_by = $currentUserId;
                    $driver->updated_by = $currentUserId;
        
                    $driver->save();
                    
                    $redirect =  redirect('/myprofile')->with('status', 'Data Anda berhasil diubah dan Anda berhasil ditambahkan ke data Pengemudi!');
                } else {
                    Driver::withTrashed()->where('employee_id', $employee->id)->first()->restore();
                    $redirect =  redirect('/myprofile')->with('status', 'Data Anda berhasil diubah dan dipulihkan dari list pengemudi!');
                }
            }
        } 
        else if($employee->title_id == $driverTitleId) {//if employee is driver before
            
            if($validated['title'] != $employee->title_id) {//  if employee title is changed
               $driver =  Driver::withTrashed()->where('employee_id', $employee->id)->first();
               $vehicle =  Vehicle::where('driver_id', $driver->id)->first();

               if($vehicle != null) {
                   $vehicle->driver_id = NULL;
                   $vehicle->save();
               }
               
               $driver->delete();
               $redirect =  redirect('/myprofile')->with('status', 'Data Pegawai berhasil diubah dan Pegawai berhasil dihapus dari list pengemudi!');
            }
        } 

       $employee->title_id = $validated["title"];
       $employee->nik = $validated["nik"];
       $employee->email = $validated["email"];
       $employee->division_id = $validated["division"];
       $employee->name = $validated["name"];
       $employee->mphone_number = $validated["mphone_number"];
       $employee->messaging_id = $validated["messaging_id"];
       $employee->address = $validated["address"];
       $employee->updated_by = $currentUserId;

       $employee->save();

       $user = User::where('id', $employee->user_id)->first();
       $user->email = $validated["email"];
       $user->name = $validated["name"];
       $user->role_id = $validated["role_id"];

       $user->save();

        return $redirect;
    }

    private function createNumber($employee) {
        $initial = $this->getInitial($employee->name);
        $date = Carbon::now()->format('dmyhi');
        $request_number = "RCR-".$initial."-".$date.$employee->id;
        return $request_number;
    }

    private function getInitial($name) {
        $nameArr = explode(" ", $name);
        $initial = "";
        foreach($nameArr as $name) {
            $initial = $initial.$name[0];
        }

        return $initial;
    }

    private function sendNotifToAdmin($request) {
        $admins = User::where('role_id', 1)->get();
        $requestor = Employee::find($request->employee_id);
        $message = "Terdapat request baru dari ".$requestor->name.
        "\nuntuk tanggal ".$request->travel_date." pukul ".$request->travel_time;

        foreach($admins as $a){
            $tele = TelegramSession::where('user_id', $a->id)->first();
            $this->sendTelegramMessage($tele->username, $message);
        }

    }
}
