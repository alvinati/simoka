<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;

use App\Models\Driver;
use App\Models\UserRequest;
use App\Models\Employee;
use App\Models\Vehicle;

class DriverController extends Controller {

    public function __invoke() {
        $user_name = Auth::user()->name;
        
        $this->updateDriverStatus();

        $drivers = Driver::with(['car', 'employee', 'status'])->orderBy('id', 'asc')->simplePaginate(5);
        $cars = Vehicle::where('driver_id', NULL)->get();
        return view('layouts.admin.driver', ['user_fullname' => $user_name, 
                        'drivers' => $drivers, 'cars'=>$cars, 'searchKey' => '']);

    }

    public function update(Request $request) {

        $validator = Validator::make($request->all(), [
            'address' => 'required|max:255',
            'name' => 'required'
        ], $messages = [
            'mphone_number.required' => 'Nomor HP tidak boleh kosong',
            'address.required' => 'Alamat tidak boleh kosong',
            'address.max' => 'Alamat tidak boleh melebihi 255 karakter',
            'name.required' => 'Nama tidak boleh kosong'
        ]);

        if ($validator->fails()) {
            return redirect('/master/pengemudi')
                ->withErrors($validator)
                ->withInput();
        }

        $savedDriver = Driver::where('id',$request->id)->first();

        if($request->car_id != NULL && $request->car_id != $savedDriver->car_id) {
            $validateCarId = Validator::make($request->all(), [
                'car_id' => 'unique:drivers'
            ], $messages = [
                'car_id.unique' => 'Kendaraan dengan plat yang diberikan sudah di assign ke pengemudi lain'
            ]);

            if ($validateCarId->fails()) {
                return redirect('/master/pengemudi')
                    ->withErrors($validateCarId)
                    ->withInput();
            }
        }

        $employee = Employee::where('id', $savedDriver->employee_id)->first();
        if($request->mphone_number != $employee->mphone_number) {
            $validatePhoneNumber = Validator::make($request->all(), [
                'mphone_number' => 'unique:employees',
            ], $messages = [
                'mphone_number.unique' => 'No HP sudah digunakan oleh pegawai lain'
            ]);

            if ($validatePhoneNumber->fails()) {
                return redirect('/master/pengemudi')
                    ->withErrors($validatePhoneNumber)
                    ->withInput();
            }
        }

        $currentUserId = Auth::user()->id;

        $employee = Employee::find($savedDriver->employee_id);
        $employee->name = $request->name;
        $employee->mphone_number = $request->mphone_number;
        $employee->address = $request->address;
        $employee->updated_by = $currentUserId;

        $employee->save();

        if($request->car_id != 0 && $request->car_id > 0){

            if($savedDriver->car_id != NULL && $request->car_id == $savedDriver->car_id)
                return redirect('/master/pengemudi')->with('status', 'Data pengemudi berhasil diubah!');

            if($savedDriver->car_id != 0){
                $oldCar = Vehicle::find($savedDriver->car_id);
                $oldCar->driver_id = NULL;
                $oldCar->updated_by = $currentUserId;
                $oldCar->save();
            }

            $car = Vehicle::find($request->car_id);
            $car->driver_id = $savedDriver->id;
            $car->updated_by = $currentUserId;
            $car->save();

            $savedDriver->car_id = $request->car_id;
            $savedDriver->updated_by = $currentUserId;
            $savedDriver->save();
        }

        return redirect('/master/pengemudi')->with('status', 'Data pengemudi berhasil diubah!');
    }

}