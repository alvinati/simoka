<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

use App\Models\UserRequest;
use App\Models\Requisite;
use App\Models\Vehicle;
use App\Models\Driver;
use App\Models\Employee;
use App\Models\Booking;
use App\Models\BookingStatus;
use App\Models\BookingReceipt;
use App\Utils\StringUtils;
use App\Utils\NumberUtils;


class RequestController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     //----------- SHOW ADMIN DASHBOARD PAGE-------------///
    public function __invoke()
    {
        $user_name = Auth::user()->name;
        $this->updateRequestsData();

        $requests = UserRequest::with(['employee', 'requisite', 'status'])
                        ->orderBy('travel_date', 'asc')
                        ->simplePaginate(5);

        return view('layouts.admin.request', ['user_fullname' => $user_name,'requests' => $requests]);
    }

    //----------SHOW VALIDATION PAGE USER REQUEST-----------------///
    public function validation($requestId, $action) {
        $user_name = Auth::user()->name;
        $drivers = NULL;

        $requestData = UserRequest::where('id', $requestId)->first();

        //if request status menunggu persetujuan or diterima (driver still able to change)
        if($requestData->status_id == 1 || $requestData->status_id == 2) {
            $this->updateDriverStatus();
        }

        if($requestData->driver_id == NULL){
            $requestData = UserRequest::with(['employee', 'requisite', 'status'])
                ->where('id', $requestId)->first();
        } else {
            $driverId = $requestData->driver_id;
            $driver = Driver::where('id', $driverId)->first();
            $driverName = Employee::where('id', $driver->employee_id)->first()->name;

            $requestData = UserRequest::with(['employee', 'requisite', 'status'])
                ->where('id', $requestId)->first();

            $requestData->{"driver_name"} = $driverName;
        };
       
        if($action == 'validasi' || $action == 'ubah'){
            $drivers = $this->findAvailableDrivers($requestData->travel_date);
        }

        $requestData->travel_date =  StringUtils::toLocalDateString($requestData->travel_date);
        
        $booking = Booking::select('book_number', 'id')->where('request_id', $requestId)->first();
        if($booking != NULL) {
            $requestData->{'booking_number'} = $booking->book_number;
            $requestData->{'booking_id'} = $booking->id;
        }

        return view('layouts.admin.validation', ['user_fullname' => $user_name,
            'data' => $requestData, 'drivers' => $drivers, 'action' => $action
        ]);
    }

    ///----------Save USER REQUEST-----------------///
    public function saveRequestChange(Request $request) {
        $userId = Auth::user()->id;

        $url = "pengajuan/".$request->id."/".$request->action;
    
        if ((int)$request->driver < 1) {
            return redirect($url)
                        ->withErrors("Driver harus dipilih untuk ".$request->action." pengajuan, silahkan pilih driver")
                        ->withInput();
        }

        //update request
        $savedData = UserRequest::where('id', $request->id)->first();
        $savedData->driver_id = $request->driver;
        $savedData->updated_by = $userId;
        $savedData->status_id = 2; // 2 means Diterima
        $savedData->save();
        $car_id = Driver::find($savedData->driver_id)->car_id;

        $this->update_driver_car($savedData->driver_id, 2, 2);

        $booking = Booking::where('request_id', $savedData->id)->first();
        if($booking == null) {   
            $this->createNewBooking($savedData, $car_id);
        }

        $result = $this->sendTelegramRequestUpdate($savedData);
        foreach($result as $r) {
            if($r != 'success') {
                return redirect($url)->with('status', "pengajuan berhasil di ".$request->action.". Namun, ".$r);
            }
        }

        return redirect($url)->with('status', 'pengajuan berhasil di '.$request->action);
    }

    ///----------DECLINE USER REQUEST-----------------///
    public function declineRequest(Request $request) {
        $userId = Auth::user()->id;

        $url = "pengajuan/".$request->id."/".$request->action;

        if(empty($request->note)) {
            return redirect($url)
                ->withErrors("Harap berikan keterangan alasan ".$request->action." pengajuan");
        }

        $savedData = UserRequest::where('id', $request->id)->first();
        $savedData->notes = $request->note;
        $savedData->updated_by = $userId;

        if($request->action == 'tolak'){
            $savedData->status_id = 3;
        } else if($request->action == 'batalkan') {
            $savedData->status_id = 6;
            $booking = Booking::where('request_id', $savedData->id)->first();
            $booking->status_id = 3;
            $booking->save();
        }
        $savedData->save();

        $this->sendTelegramDeclinedRequest($savedData);
        return redirect($url)->withErrors('Pengajuan berhasil '.$request->action);
    }

    private function findAvailableDrivers($travel_date) {
     

        if(Carbon::today()->eq(Carbon::parse($travel_date))) { //if request for today travel date
            $drivers = DB::table('drivers')
                    ->join('employees', 'drivers.employee_id', '=','employees.id')
                    ->where('drivers.availability_id', '=', 1) //where driver vacant
                    ->where('drivers.status_id','=', 6) // where driver masuk kerja
                    ->where('drivers.car_id', '>=', 1) //where driver has assigned a car
                    ->select('drivers.id', 'employees.name')
                    ->get();
            return $drivers;
        }

        $requests = UserRequest::where('travel_date', $travel_date)
                            ->where('driver_id', '!=', null)
                            ->where('status_id', 2)
                            ->get();  

        $drivers = DB::table('drivers')
                    ->join('employees', 'drivers.employee_id', '=','employees.id')
                    ->where('drivers.car_id', '>=', 1) //where driver has assigned a car
                    ->select('drivers.id', 'employees.name')
                    ->get();
        
        $driverIds = array();

        if(count($requests) > 0) {
            foreach($requests as $r) {
                array_push($driverIds, $r->driver_id);
            }
        }
    
        if($drivers != null) {    
           $drivers =  $drivers->whereNotIn('id',$driverIds);
        }

        return $drivers;
    }


    //create new booking
    private function createNewBooking($request, $car_id) {
         $newBooking = new Booking;
         $newBooking->book_number = str_replace("RCR", "BK", $request->request_number)."-RQ".$request->id;
         $newBooking->employee_id = $request->employee_id;
         $newBooking->request_id = $request->id;
         $newBooking->car_id = $car_id;
         $time_val = strtotime("$request->travel_date $request->travel_time");
         $newBooking->exit_date = date('Y-m-d h:i:s', $time_val);
         $newBooking->status_id = 1;
         $newBooking->save();
    }


    //---------- UPDATE DRIVER AND CAR STATUS -------------------//
    private function update_driver_car($driverId, $driverNewStatus, $carNewStatus) {
        $userId = Auth::user()->id;

        //update driver
        $driver = Driver::where('id', $driverId)->first();
        $driver->availability_id = $driverNewStatus;
        $driver->updated_by = $userId;
        $driver->save();

        //update car
        $car = Vehicle::where('id', $driver->car_id)->first();
        $car->status_id = $carNewStatus;
        $car->updated_by = $userId;
        $car->save();
    }

    private function sendTelegramRequestUpdate($request) {
        $driver = Driver::find($request->driver_id);
        $employeeDriver = Employee::find($driver->employee_id);
        $employeeRequestor = Employee::find($request->employee_id);

        $messageToDriver =
        "Halo ".$employeeDriver->name.
        "\nAnda ditugaskan untuk mengantar ".$employeeRequestor->name.
        "\nTujuan: ".$request->destination.
        "\nTanggal: ".$request->travel_date.
        "\nJam: ".$request->travel_time.
        "\nNomor yang bisa dihubungi: ".$employeeRequestor->mphone_number.
        "\nTerima kasih, Selamat bekerja :)
        ";

        $messageToRequestor = 
        "Pengajuan Anda untuk tanggal pergi ".$request->travel_date.
        ", dengan tujuan ".$request->destination.
        ", driver yang ditugaskan adalah ".$employeeDriver->name.
        "\n\nNo Hp. ".$employeeDriver->mphone_number;

        $d = $this->sendTelegramMessage($employeeDriver->messaging_id, $messageToDriver);
        $e = $this->sendTelegramMessage($employeeRequestor->messaging_id, $messageToRequestor);

        return array($d, $e);
    }

    private function sendTelegramDeclinedRequest($request) {
        $employeeRequestor = Employee::find($request->employee_id);
        
        $messageToRequestor = 
        "Mohon maaf, Pengajuan Anda ditolak / dibatalkan, dengan alasan:".
        "\n".$request->notes;

        $e = $this->sendTelegramMessage($employeeRequestor->messaging_id, $messageToRequestor);
        return $e;
    }
}
