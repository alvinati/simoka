<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use WeStacks\TeleBot\TeleBot;
use Carbon\Carbon;

use App\Models\TelegramSession;
use App\Models\UserRequest;
use App\Models\DriverPresence;
use App\Models\Driver;
use App\Models\Booking;

use App\Utils\StringUtils;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendTelegramMessage($username, $text) {
        $teleSession = TelegramSession::where('username', $username)->first();

        if($teleSession == null) {
            return 'Pegawai dengan username Telegram '.$username.' belum aktifasi telegram dengan chat pertama kali ke @SimokaapBot';
        }

        $chat_id = $teleSession->chat_id;

        if($chat_id == null) {
            return 'Chat ID dari pegawai dengan username Telegram '.$username.' tidak ditemukan';
        }

        $bot = new TeleBot(env('TELEGRAM_BOT_TOKEN'));
        $bot->sendMessage([
            'chat_id' => $chat_id,
            'text' => $text
        ]);

        return 'success';
    }

    public function sendTelegramMessageToAdmin($message) {
        $admins = User::where('role_id', 1)->get();

        foreach($admins as $admin) {  
            $teleSess = TelegramSession::where('user_id', $admin->id)->first();
            if($teleSess != null) {
                $username = $teleSess->username;
                $this->sendTelegramMessage($username, $message);
            }
        }
    }

    public function updateRequestsData() {

        $requests = UserRequest::where('status_id', 1)
                        ->where('travel_date','<=', Carbon::today())
                        ->get();

        foreach($requests as $r) {
            
            if($r->status_id == 1 && strtoTime("$r->travel_date $r->travel_time") < time()) { // if waiting and outdated
                $r->status_id = 5; //5 means outdated
                $r->save();
            }

            $r->travel_date = StringUtils::toLocalDateString($r->travel_date);
            
        }

        return $requests;
    }

    public function updateDriverStatus() {

        //check driver presence is already absen or not
        $driverAbsens = DriverPresence::whereDate('created_at', Carbon::today())->get()->pluck('driver_id');
        $drivers = Driver::all()->whereNotIn('id', $driverAbsens); //yang belum absen

        foreach($drivers as $d) {
            $d->status_id = 7;
            $d->save();
        }

        //check if any driver assigned for today bookings, update driver availability to booked
        $acceptedRequest = UserRequest::select('driver_id')->where('driver_id','!=', null)
                        ->where('travel_date', Carbon::today())
                        ->where('status_id', 2) //2 means Diterima
                        ->get();

        $todayBookedDrivers = array();
        if($acceptedRequest != null && count($acceptedRequest)>0) {
            foreach($acceptedRequest as $r) {
                $driver = Driver::where('id', $r->driver_id)->first();
                array_push($todayBookedDrivers, $r->driver_id);
                if($driver != null) {
                    $driver->availability_id = 2; //2 means Booked
                    $driver->save(); //update today assigned to booked
                } 
            }
        }

        $drivers = Driver::whereNotIn('id', $todayBookedDrivers)->get();
        foreach($drivers as $d) {
            $d->availability_id = 1;
            $d->save(); //update others as vacant
        }
    }
}
