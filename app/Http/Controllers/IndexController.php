<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Models\User;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Password;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;

class IndexController extends Controller
{
    public function __invoke() {
        // $isAny = File::exists(storage_path('/app/public/public.pem'));
        // // dd(storage_path('public.pem')); 
        // dd($isAny);
        return view('welcome');
    }

    
}
