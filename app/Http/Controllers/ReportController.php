<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use Illuminate\Support\Facades\Auth;

use App\Exports\ReportExport;
use Maatwebsite\Excel\Facades\Excel;

use App\Models\UserRequest;
use App\Models\Requisite;
use App\Models\Driver;
use App\Models\Vehicle;
use App\Models\Employee;
use App\Models\Booking;
use App\Utils\StringUtils;
use App\Utils\NumberUtils;



class ReportController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $now = Carbon::now();
        $currentMonth = $now->month;
        $currentYear = $now->year;

        $reportTableView = $this->getReportTable($currentMonth, $currentYear);

        return $reportTableView;
        
    }

    public function showFiltered(Request $request) {
        return $this->getReportTable($request->month, $request->year);

    }

    public function reportDetail($id) {
        $user = Auth::user();

        $b = Booking::with(['request'])->where('id', $id)->first();

        $b->exit_date = StringUtils::toLocalDateString($b->exit_date);
        $b->return_date = StringUtils::toLocalDateString($b->return_date);
        $b->fuel_cost = NumberUtils::toStringRupiah($b->fuel_cost);
        $b->toll_cost = NumberUtils::toStringRupiah($b->toll_cost);
        $b->total_cost = NumberUtils::toStringRupiah($b->total_cost);

        $requisite = Requisite::select('requisite')->where('id', $b->request->requisite_id)->first();
        $employee = Employee::with(['division','title'])
                                    ->where('id', $b->request->employee_id)
                                    ->first();
        $b->{'employee'} = $employee;
        $driver = Driver::with(['car', 'employee'])->where('id', $b->request->driver_id)->first();
        $b->request->{'requisite'} = $requisite->requisite;
        $b->{'requestor'} = $employee->name;
        $b->{'driver'} = $driver->employee->name;
        $car = Vehicle::withTrashed()->where('id', $b->car_id)->get()->first();
        $b->{'car'} = $car->plat_number;

        return view('layouts.admin.reportdetail', ['user_fullname' => $user->name, 'user_role' => $user->role_id,
                                        'data' => $b]);
    }

    public function exportExcel(Request $request) {
        return Excel::download(new ReportExport($request->month, $request->year), 'Laporan-'.$request->month.$request->year.'.xlsx');
    }

    private function getReportTable($month, $year) {

        $user = Auth::user();
        
        // if($user->role_id == 1) { // if show list for admin
        $bookings = Booking::with(['request', 'status'])
                    ->whereYear('created_at', '=', $year)
                    ->whereMonth('created_at', '=', $month)
                    ->where('status_id', 2) //2 means finished
                    ->orderBy('return_date', 'desc')
                    ->simplePaginate(30);
        // } else {
        //     $employee = Employee::select('id')->where('user_id', $user->id)->first();
        //     $bookings = Booking::with(['request', 'status'])
        //                     ->whereYear('created_at', '=', $year)
        //                     ->whereMonth('created_at', '=', $month)
        //                     ->where('employee_id', $employee->id)
        //                     ->orderBy('return_date', 'desc')
        //                     ->simplePaginate(30);
        // }

        if(count($bookings) > 0) {
            foreach($bookings as $key=>$b) {
                $b->exit_date = StringUtils::toLocalDateString($b->exit_date);
                $b->return_date = StringUtils::toLocalDateString($b->return_date);
    
                $requisite = Requisite::select('requisite')->where('id', $b->request->requisite_id)->first();
                $employee = Employee::select('name')->where('id', $b->request->employee_id)->first();
                $driverEmployee = Driver::select('employee_id')->where('id', $b->request->driver_id)->first();
                $driver = Employee::select('name')->where('id', $driverEmployee->employee_id)->first();
                $b->request->{'requisite'} = $requisite->requisite;
                $b->{'requestor'} = $employee->name;
                $b->{'driver'} = $driver->name;
                $b->total_cost = NumberUtils::toStringRupiah($b->total_cost);
            };
        }

        $months = $this->getMonths();
        $years = $this->getFiveYearsBeforeAndAfter($year);

        return view('layouts.admin.report', ['user_fullname' => $user->name, 'user_role' => $user->role_id, 
                                        'bookings' => $bookings, 'months' => $months, 'years' => $years, 'currentYear' => $year, 'currentMonth' => $month]);
    }

    private function getFiveYearsBeforeAndAfter($year) {
        $years = array();

        for($y = 5; $y>0; $y--) {
            array_push($years, $year-$y);
        }
        array_push($years, $year);
        for($y = 1; $y<6; $y++) {
            array_push($years, $year+$y);
        }

        return $years;
    }

    private function getMonths() {
        return array(1 => 'Januari', 2 => 'Februari', 3=>'Maret', 4=>'April', 5=>'Mei', 6=>'Juni',
        7 => 'Juli', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11=> 'November', 12 => 'Desember');
    }
}