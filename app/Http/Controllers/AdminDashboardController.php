<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

use App\Models\UserRequest;
use App\Models\Requisite;
use App\Models\Vehicle;
use App\Models\Driver;
use App\Models\Employee;
use App\Models\Booking;
use App\Models\BookingStatus;
use App\Models\BookingReceipt;
use App\Utils\StringUtils;
use App\Utils\NumberUtils;


class AdminDashboardController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     //----------- SHOW ADMIN DASHBOARD PAGE-------------///
    public function __invoke()
    {
        $user_name = Auth::user()->name;
        $now = Carbon::now();
        $date = $now->locale('id')->isoFormat('LLLL');

        $this->updateRequestsData();
        $this->updateDriverStatus();

        $requests = UserRequest::with(['employee'])->whereDate('created_at',Carbon::today())->orderBy('travel_date', 'asc')->get();
        $drivers = Driver::with(['car', 'employee', 'status', 'availability'])->orderBy('id', 'asc')->get();

        return view('layouts.admin.dashboard', 
                    ['user_fullname' => $user_name, 'currentDate' => $date, 
                    'requests'=>$requests, 'drivers' => $drivers]);
    }
}