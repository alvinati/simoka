<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Models\Employee;
use App\Models\PasswordToken;
use App\Models\TelegramSession;
use Carbon\Carbon;

class AuthController extends Controller {

    public function __invoke() {
        if(Auth::check()) {
            $userRole = Auth::user()->role_id;
                
            if($userRole == 1) { //role id in user_role table where role = admin
                return redirect('/admin');
            } else {
                return redirect('/dashboard');
            }
        }

        return view('auth.login');
    }

    
    public function doLogin(Request $request) {

        $email = $request->email;
        $password = $request->password;
        $remember = $request->remember;

        if (Auth::attempt(['email' => $email, 'password' => $password], $remember)) {

            if(Auth::user()->login_first_time == NULL) {
                $userid = Auth::user()->id;
                $anyTele = TelegramSession::where('user_id', $userid)->first();
                $employee = Employee::where('user_id', $userid)->first();

                $passToken = new PasswordToken;
                $token = bin2hex(random_bytes(10));
                $passToken->add($employee->email, $token);
               
                $this->clearSession($request);
                
                if(isset($employee) && $employee->messaging_id == null) {
                    return redirect('ganti-password/'.$token)
                        ->with('warning','username Telegram Anda belum terdaftar, silahkan hubungi Admin untuk mendaftarkan username Telegram Anda terlebih dahulu');

                } else if(!isset($anyTele)) {
                    return redirect('ganti-password/'.$token)
                        ->with('warning','Akun Telegram Anda belum terverifikasi, silahkan chat @SimokappBot /start untuk verifikasi Telegram');
                }
                
                return redirect('ganti-password/'.$token);
            }

            $userRole = Auth::user()->role_id;
                
            if($userRole == 1) { //role id in user_role table where role = admin
                return redirect('/admin');
            } else {
                return redirect('/dashboard');
            }
          
        }

        return back()->withErrors([
            'email' => 'Email/Password yang diberikan belum sesuai'
        ]);
    }

    public function logout(Request $request)
    {
       $this->clearSession($request);

        return redirect('/login');
    }

    private function clearSession(Request $request) {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();
    }

    public function reqResetPassword(Request $request) {
        $username = $request->uname;

        $employee = Employee::where('messaging_id', $username)->first();
        
        if($employee == null) {
            return redirect('/lupa-password')->withErrors(['Mohon maaf, username Telegram Anda belum terdaftar']);
        } else if($employee->messaging_id == null){
            return redirect('/lupa-password')->withErrors(['Mohon maaf, username Telegram Anda belum terdaftar']);
        } 
        else {
            $anyTele = TelegramSession::where('user_id', $employee->user_id)->first();
            if(!isset($anyTele)) {
                return redirect('/lupa-password')->withErrors(['Mohon maaf, akun Telegram Anda belum terverifikasi, silahkan chat SimokappBot /start terlebih dahulu']);
            }

            $passToken = new PasswordToken;
            $token = bin2hex(random_bytes(10));
            $passToken->add($employee->email, $token);
            
            $result = $passToken->sendResetLink($username, $token);;
            if($result == 'success') {
                return redirect('/login')->with('status','Link sudah dikirimkan, silahkan cek Telegram Anda');
            } else {
                return redirect('lupa-password')->withErrors([$result]);
            }
        }
    }

    public function showChangePassword($token) {
        $passToken = PasswordToken::where('token', $token)->first();

        if($passToken == null) {
            return redirect('login')->withErrors(['Token ganti password tidak ditemukan / expired']);
        }
        $employee = Employee::where('email', $passToken->email)->first();
        if($employee == null) {
            return  redirect('login')->withErrors(['Pegawai tidak ditemukan']);
        }

        $user = User::where('id',$employee->user_id)->first();
        if($user == null) {
            return  redirect('login')->withErrors(['Pegawai tidak ditemukan']);
        }

        if($user->login_first_time == NULL) {
            return view('auth.reset-password', ['message' => 'Silahkan buat kata sandi baru untuk akun Anda', 'token' => $token, 'id' => $user->id]);
        }

        return view('auth.reset-password', ['token' => $token, 'id' => $user->id]);
    }

    public function changePassword(Request $request) {

        $validated = Validator::make($request->all(), [
            'id' => 'required',
            'password' => 'required|string|min:8',
            'repeatPassword' => 'required'
        ] , [
            'id.required' => 'Pegawai tidak ditemukan',
            'password.required' => 'Kata Sandi tidak boleh kosong',
            'repeatPassword.required' => 'Ulangi Kata Sandi tidak boleh kosong',
            'password.min' => 'Password minimal 8 Karakter'
        ]);

        if($validated->fails()) {
            return back()
                ->withErrors($validated)
                ->withInput();
        }

        $user = User::where('id',$request->id)->first();
        $password = Hash::make($request->password);
        $user->password = $password;
        if(!isset($user->login_first_time)) {
            $user->login_first_time =  Carbon::now()->format('Y-m-d H:i:s');
        }
        $user->save();
        $passToken = PasswordToken::where('token', $request->token)->first()->delete();
        return redirect('login')->with('status', 'Password berhasil diubah, Silahkan Login dengan password baru');
    }

    public function updatePassword(Request $request) {
        $newPassword = $request->input('newPassword');
        $repeatPassword = $request->input('repeatPassword');
        return "newpass: ".$newPassword." repeat pass: ".$repeatPassword;
    }

}