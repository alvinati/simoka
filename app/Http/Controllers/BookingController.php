<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

use App\Models\UserRequest;
use App\Models\Requisite;
use App\Models\Vehicle;
use App\Models\Driver;
use App\Models\Employee;
use App\Models\Booking;
use App\Models\BookingStatus;
use App\Models\BookingReceipt;
use App\Utils\StringUtils;
use App\Utils\NumberUtils;


class BookingController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     
    ///----------SHOW BOOKING LISTS-----------------///
    public function __invoke()
    {
        $user_name = Auth::user()->name;

        $bookings = Booking::with(['request','status'])
                        ->orderBy('exit_date', 'desc')
                        ->simplePaginate(5);

        // Storage::delete('fuels/p8IUtfzqJQHKmtJHv37xtGvtsI1GX0mKd2sX9Er0.jpg');
        // $files = Storage::allFiles('/fuels');
        // var_dump($files);return;
  
        if(count($bookings) > 0) {
            foreach($bookings as $key=>$b) {

                $b->{'isCancelable'} = $b->status_id < 2 && $b->exit_date >= Carbon::now();
               
                $b->exit_date = StringUtils::toLocalDateString($b->exit_date);
               
                if(isset($b->return_date))
                    $b->return_date = StringUtils::toLocalDateString($b->return_date);
    
                $requisite = Requisite::select('requisite')->where('id', $b->request->requisite_id)->first();
                $employee = Employee::select('name')->where('id', $b->request->employee_id)->first();
                $b->request->{'requisite'} = $requisite->requisite;
                $b->{'requestor'} = $employee->name;
            };
        }
       
        return view('layouts.admin.booking', ['user_fullname' => $user_name, 'bookings' => $bookings]);
    }


    ///----------SHOW BOOKING DETAIL-----------------///
    public function detailBooking($id) {
        $user_name = Auth::user()->name;

        $data = Booking::with(['request', 'status'])->where('id', $id)->first();
        $driver = Driver::select('employee_id', 'car_id')->where('id', $data->request->driver_id)->first();
        $requisite = Requisite::select('requisite')->where('id', $data->request->requisite_id)->first();
        $car = Vehicle::withTrashed()->where('id', $data->car_id)->first();
        $driverName = Employee::select('name')->where('id', $driver->employee_id)->first();
        $requestor = Employee::select('name')->where('id', $data->request->employee_id)->first();

        $data->request->travel_date = StringUtils::toLocalDateString($data->request->travel_date);
    
        $data->{'car'} = $car->plat_number;
        if($data->status_id ==1) { //if status booking == dalam proses -> show as number
            $data->{'km'} = $car->km;
        }else { //show as formated number
            $data->{'km'} = number_format($car->km,2,',','.');
        }

        $data->{'requisite'} = $requisite->requisite;
        $data->{'driver_name'} = $driverName->name;
        $data->{'requestor'} = $requestor->name;

        if($data->status->id == 2) { //if status booking = selesai -> add receipts info
            $fuel_receipts = BookingReceipt::select('image_path')
                                ->where('booking_id', $data->id)
                                ->where('type', 'FUEL')
                                ->get();

            $data->{'fuel_receipt_paths'} =$fuel_receipts;

            $data->{'fuel_cost_string'} = NumberUtils::toStringRupiah($data->fuel_cost);

            if($data->toll_cost > 0) {
                $toll_receipts = BookingReceipt::select('image_path')
                                    ->where('booking_id', $data->id)
                                    ->where('type', 'TOLL')
                                    ->get();
                                    
                if(count($toll_receipts) > 0) {
                    $data->{'toll_receipt_paths'} = $toll_receipts;
                }

                $data->{'toll_cost_string'} = NumberUtils::toStringRupiah($data->toll_cost);
            }

            $data->{'total_cost_string'} = NumberUtils::toStringRupiah($data->total_cost);
        }

        return view('layouts.admin.finishbook', ['user_fullname' => $user_name, 'data' => $data]);
    }

    ///----------CANCEL BOOKING USER REQUEST-----------------///
    public function cancelBooking(Request $request) {

        if($request->notes === NULL) {
            return redirect('/peminjaman')
                ->withErrors(['Mohon isi keterangan pembatalan']);
        }
      
        $booking = Booking::where('id',$request->id)->first();
        $booking->status_id = 3; //means dibatalkan

        $userRequest = UserRequest::where('id',$booking->request_id)->first();
        $userRequest->status_id = 6; //means dibatalkan
        $userRequest->notes = $request->notes;

        $userRequest->save();
        $booking->save();

        $this->update_driver_car($userRequest->driver_id, 1, 1);

        $this->sendTelegramCancelBooking($userRequest);
        return redirect('/peminjaman')->with('status', 'Peminjaman Dibatalkan');
    }

    ///----------FINISH USER BOOKING AND SAVE RECEIPTS-----------------///
    public function finishbooking(Request $request) {
        $userId = Auth::user()->id;
        $minCost = 2000;
       
        $validator = Validator::make($request->all(), [
            'fuel' => 'required|numeric|min:2000',
            'fuel_receipts.*' => 'image|max:2000',
            'toll_receipts.*' => 'image|max:2000'
        ], $messages = [
            'fuel.required' => 'Biaya bensin harus di isi',
            'fuel.min' => 'Harap isi biaya bensin dengan jumlah yang sesuai',
            'fuel_receipts.*' => ['image'=>'File struk bensin yang di upload harus berupa foto'],
            'toll_receipts.*' => ['image'=>'File struk pembayaran toll yang di upload harus berupa foto'],
            'toll_receipts.*' => ['max'=>'Ukuran foto yang di upload tidak boleh melebihi 2 MB'],
            'fuel_receipts.*' => ['max'=>'Ukuran foto yang di upload tidak boleh melebihi 2 MB']
        ]);

        if ($validator->fails()) {
            return redirect('peminjaman/'.$request->id)
                        ->withErrors($validator)
                        ->withInput();
        }
            
        if(!$request->hasFile('fuel_receipts')) {
            return redirect('peminjaman/'.$request->id)
                    ->withErrors('Harap sertakan struk pembayaran bensin');
        }
         
        if($request->toll > 0 && !$request->hasFile('toll_receipts')) {
            return redirect('peminjaman/'.$request->id)
                    ->withErrors('Harap sertakan juga struk pembayaran toll');
        }

        if($request->hasFile('toll_receipts') && $request->toll < $minCost) {
            return redirect('peminjaman/'.$request->id)
                    ->withErrors('Harap isi biaya toll dengan jumlah yang sesuai');
        }

        //save booking info
        $booking = Booking::where('id', $request->id)->first();
        $booking->return_date = Carbon::now();
        $booking->fuel_cost = $request->fuel;
        $booking->toll_cost = $request->toll;
        $booking->total_cost = $request->total;
        $booking->status_id = 2; //where 2 means finished
        $booking->save();

        //save fuel receipt image path to db
        foreach(array_slice($request->fuel_receipts, 0, 3) as $f ) {
            $path = $f->store('fuel');
            $fuel_receipt = new BookingReceipt;
            $fuel_receipt->booking_id = $booking->id;
            $fuel_receipt->image_path = $path;
            $fuel_receipt->type = "FUEL";
            $fuel_receipt->save();
        }

        //save toll receipt image path if any to db
        if($request->hasFile('toll_receipts')) {
            foreach(array_slice($request->toll_receipts, 0, 3) as $t ) {
                $path = $t->store('toll');
                $toll_receipt = new BookingReceipt;
                $toll_receipt->booking_id = $booking->id;
                $toll_receipt->image_path = $path;
                $toll_receipt->type = "TOLL";
                $toll_receipt->save();
            }
        }

        $request = UserRequest::where('id', $booking->request_id)->first();
        $request->status_id = 4;
        $request->updated_by = $userId;
        $request->save();

        $this->update_driver_car_km($request->driver_id, 1, 1, $request->km);

        return redirect('peminjaman/'.$booking->id)->with('status', "Peminjaman".$booking->booking_number." berhasil diselesaikan");
    }


    //---------- UPDATE DRIVER AND CAR STATUS -------------------//
    private function update_driver_car($driverId, $driverNewStatus, $carNewStatus) {
        $this->update_driver_car_km($driverId, $driverNewStatus, $carNewStatus, 0);
    }

    
    //---------- UPDATE DRIVER AND CAR STATUS + CAR KM -------------------//
    private function update_driver_car_km($driverId, $driverNewStatus, $carNewStatus, $carKm) {
           $userId = Auth::user()->id;

           //update driver
           $driver = Driver::where('id', $driverId)->first();
           $driver->availability_id = $driverNewStatus;
           $driver->updated_by = $userId;
           $driver->save();
   
           //update car
           $car = Vehicle::where('id', $driver->car_id)->first();
           $car->status_id = $carNewStatus;
           $car->updated_by = $userId;
           if($carKm > $car->km) {
               $car->km = $carKm;
           }
           $car->save();
    }

    private function sendTelegramCancelBooking($request) {
        $employee = Employee::where('id',$request->employee_id)->first();
        $messageToRequestor = 
        "Mohon maaf, Pengajuan Anda di *batalkan*, dengan alasan:".
        "\n".$request->notes;

        $e = $this->sendTelegramMessage($employee->messaging_id, $messageToRequestor);
        return $e;
    }
}
