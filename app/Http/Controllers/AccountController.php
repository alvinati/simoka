<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Models\Employee;
use App\Models\EmployeeDivision;
use App\Models\EmployeeTitle;

class AccountController extends Controller
{
    public function __invoke() {
        $user = Auth::user();

        $employee = Employee::select('mphone_number')->where('user_id', $user->id)->first();
        $user->{'mphone_number'} = $employee->mphone_number;
        return view('layouts.admin.profile', ['user_fullname' => $user->name,
                    'user' => $user]);
    }

    public function updateProfile(Request $request) {
        
        $validation = Validator::make($request->all(),[
            'name' => 'required|string|max:100',
            'email' => 'required|string|max:100',
            'mphone_number' => 'required|numeric|digits_between:10,14|starts_with:08'
        ], $messages = [
            'name.required' => "nama tidak boleh kosong",
            'email.required' => 'email tidak boleh kosong',
            'mphone_number.required' => 'no HP tidak boleh kosong',
            'mphone_number.digits_between' => 'Nomor HP minimal 10 karakter dan maksimal 14 karakter',
            'mphone_number.starts_with' => 'Nomor HP tidak valid, isi dengan awalan 08',
            'mphone_number.numeric' => 'Isi nomor HP dengan angka',
        ]);

        if($validation->fails()) {
            return redirect('/profil')
            ->withErrors($validation)
            ->withInput();
        }
        
        $currentUser = User::where('id', $request->id)->first();
        $employee = Employee::where('user_id', $request->id)->first();

        if($currentUser->email != $request->email) {
            $emailUniqueValidator = Validator::make($request->all(),[
                'email' => 'unique:employees',
            ], $messages = [
                'email.unique' => "Sudah ada pengguna dengan email yang sama"
            ]);

            if($emailUniqueValidator->fails()) {
                return redirect('/profil')
                ->withErrors($emailUniqueValidator)
                ->withInput();
            }
        }

        if($employee->mphone_number != $request->mphone_number) {
            $phoneUniqueValidator = Validator::make($request->all(), [
                'mphone_number' => 'unique:employees',
            ], $messages = [
                'mphone_number.unique' => "Sudah ada pengguna dengan nomor hp yang sama"
            ]);

            if($phoneUniqueValidator->fails()) {
                return redirect('/profil')
                ->withErrors($phoneUniqueValidator)
                ->withInput();
            }
        }

        $currentUser->name = $request->name;
        $currentUser->email = $request->email;
        $employee->name = $request->name;
        $employee->email = $request->email;
        $employee->mphone_number = $request->mphone_number;
      
        $currentUser->save();
        $employee->save();

        return redirect('/profil')->with('status', 'Perubahan Akun Berhasil Disimpan!');
    }

    public function updatePassword(Request $request) {

        if($request->newPassword != $request->repeatPassword) {
            return redirect('/profil')->withErrors(["Kata sandi dan ulangi kata sandi tidak sama"]);
        }

        $validator = Validator::make($request->all(), [
            'newPassword' => 'required|string|min:8',
            'repeatPassword' => 'required|string'
        ], $messages = [
            'newPassword.required' => "Kata sandi baru tidak boleh kosong",
            'newPassword.min' => "Kata sandi minimal 8 karakter",
            'repeatPassword.required' => "Ulangi kata sandi tidak boleh kosong"
        ]);

        if($validator->fails()) {
            return redirect('profil')
                ->withErrors($validator)
                ->withInput();
        }

        $user = Auth::user();
        $user->password = Hash::make($request->newPassword);
        $user->save();

        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/login')->with('status', 'Kata Sandi berhasil diubah, silahkan login kembali');
    }
}