<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Database\QueryException;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

use App\Http\Requests\EmployeeRequest;
use App\Models\Employee;
use App\Models\EmployeeTitle;
use App\Models\EmployeeDivision;
use App\Models\UserRole;
use App\Models\User;
use App\Models\Driver;
use App\Models\Vehicle;
use App\Models\UserRequest;
use App\Models\TelegramSession;


class EmployeeController extends Controller
{
    const EMPLOYEE_MODEL_NAME = 'employee';
    const USER_MODEL_NAME = 'user';
    const DRIVER_MODEL_NAME = 'driver';
    const TELEGRAM_SESSION_MODEL_NAME = 'telegramsession';

    public function __invoke()
    {
        $user_name = Auth::user()->name;

        $employees = Employee::with(['division', 'title', 'user']) 
                                ->where('id', '!=', 1)
                                ->orderBy('id', 'asc')
                                ->simplePaginate(5);
        $divisions = EmployeeDivision::all();
        $titles = EmployeeTitle::all();
        $roles = UserRole::all();

        return view('layouts.admin.employee', ['user_fullname' => $user_name, 
                        'employees' => $employees, 'divisions' => $divisions, 'titles' => $titles, 
                        'roles' => $roles,'searchKey' => '']);
    }

    public function save(EmployeeRequest $request) {

        $currentUserId = Auth::user()->id;
        $validated = $request->validated();

        $anyRegisteredBefore = $this->tryRestoreEmployee($request);

        if(isset($anyRegisteredBefore)) {
            return redirect('/master/pegawai')->with('status', 'Pegawai dengan '.$anyRegisteredBefore." tersebut sudah terdaftar sebelumnya, data dipulihkan, silahkan disesuaikan kembali");
        }


        $additionalValidator = Validator::make($request->all(), [
            'nik' => 'unique:employees',
            'email' => 'unique:employees|unique:users',
            'role_id' => 'required',
            'mphone_number' => 'unique:employees',
            'messaging_id' => 'unique:employees'
        ], $messages = [
            'nik.unique' => 'Karyawan dengan NIK yang diberikan sudah terdaftar',
            'email.unique' => 'Karyawan / User dengan email yang diberikan sudah terdaftar',
            'role_id.required' => 'Role user karyawan harus dipilih',
            'mphone_number.unique' => 'No HP sudah terdaftar',
            'messaging_id.unique' =>  'Username telegram sudah terdaftar'
        ]);

        if ($additionalValidator->fails()) {
            return redirect('master/pegawai')
                        ->withErrors($additionalValidator)
                        ->withInput();
        }

        $defaultPassword = "Simoka21";

        $result =  User::create([
            'name' => $validated['name'],
            'email' => $validated['email'],
            'password' => Hash::make($defaultPassword),
            'role_id' => $request->role_id
        ]);

        $createdUserId = User::where('email',$validated['email'])->first()->id;

        $employee = new Employee;
        $employee->nik = $validated["nik"];
        $employee->user_id = $createdUserId;
        $employee->title_id = $validated["title"];
        $employee->division_id = $validated["division"];
        $employee->name = $validated["name"];
        $employee->email = $validated["email"];
        $employee->mphone_number =  $validated["mphone_number"];
        $employee->messaging_id =  $validated["messaging_id"];
        $employee->address = $validated["address"];
        $employee->created_by = $currentUserId;
        $employee->updated_by = $currentUserId;

        $employee->save();

        if($validated['title'] == EmployeeTitle::where('name', 'Driver')->first()->id) {
            $driver = new Driver;
            $driver->employee_id = $employee->id;
            $driver->status_id = 7;
            $driver->updated_by = $currentUserId;
            $driver->created_by = $currentUserId;

            $driver->save();
            
            return redirect('/master/pegawai')->with('status', 'Data Pegawai dan Pengemudi berhasil ditambahkan!');
        }

        return redirect('/master/pegawai')->with('status', 'Pegawai berhasil ditambahkan!');
    }


    public function update(EmployeeRequest $request) {

        $currentUserId = Auth::user()->id;
        $validated = $request->validated();
        $employee = Employee::find($request->id);
        $redirect = redirect("master/pegawai")->with('status', 'Perubahan berhasil!');

       if($request->email != $employee->email) {
            $additionalValidator = Validator::make($request->all(), [
                'email' => 'unique:employees|unique:users'
            ], $messages = [
                'email.unique' => 'Karyawan / User dengan email yang diberikan sudah terdaftar'
            ]);
        
            if ($additionalValidator->fails()) {
                return redirect('master/pegawai')
                        ->withErrors($additionalValidator)
                        ->withInput();
            }
       }

       if($request->nik != $employee->nik) {
            $additionalValidator = Validator::make($request->all(), [
                'nik' => 'unique:employees'
            ], $messages = [
                'nik.unique' => 'Karyawan dengan NIK yang diberikan sudah terdaftar'
            ]);
            if ($additionalValidator->fails()) {
                return redirect('master/pegawai')
                            ->withErrors($additionalValidator)
                            ->withInput();
            }
        }

        $driverTitleId = EmployeeTitle::where('name', 'Driver')->first()->id;
        if($validated['title'] == EmployeeTitle::where('name', 'Driver')->first()->id) { //if current request title is driver
            
            if($validated['title'] != $employee->title_id) {//  if employee title is changed
                if(Driver::withTrashed()->where('employee_id', $employee->id)->first() == NULL) { //and if employee id is not exist as drivers table before
                    $driver = new Driver;
                    $driver->employee_id = $employee->id;
                    $driver->status_id = 1;
                    $driver->created_by = $currentUserId;
                    $driver->updated_by = $currentUserId;
        
                    $driver->save();
                    
                    $redirect =  redirect('master/pegawai')->with('status', 'Data Anda berhasil diubah dan Anda berhasil ditambahkan ke data Pengemudi!');
                } else {
                    Driver::withTrashed()->where('employee_id', $employee->id)->first()->restore();
                    $redirect =  redirect('master/pegawai')->with('status', 'Data Anda berhasil diubah dan dipulihkan dari list pengemudi!');
                }
            }
        } 
        else if($employee->title_id == $driverTitleId) {//if employee is driver before
            
            if($validated['title'] != $employee->title_id) {//  if employee title is changed
               $driver =  Driver::withTrashed()->where('employee_id', $employee->id)->first();
               $vehicle =  Vehicle::where('driver_id', $driver->id)->first();

               if($vehicle != null) {
                   $vehicle->driver_id = NULL;
                   $vehicle->save();
               }
               
               $driver->delete();
               $redirect =  redirect('master/pegawai')->with('status', 'Data Pegawai berhasil diubah dan Pegawai berhasil dihapus dari list pengemudi!');
            }
        } 

       $employee->title_id = $validated["title"];
       $employee->nik = $validated["nik"];
       $employee->email = $validated["email"];
       $employee->division_id = $validated["division"];
       $employee->name = $validated["name"];
       $employee->mphone_number = $validated["mphone_number"];
       $employee->messaging_id = $validated["messaging_id"];
       $employee->address = $validated["address"];
       $employee->updated_by = $currentUserId;

       $employee->save();

       $user = User::where('id', $employee->user_id)->first();
       $user->email = $validated["email"];
       $user->name = $validated["name"];
       $user->role_id = $validated["role_id"];

       $user->save();

        return $redirect;
    }

    
    public function delete($id) {

        $currentUserId = Auth::user()->id;

        //list of posible data relations
        $employee = Employee::find($id);
        $user = User::find($employee->user_id);
        $driver = Driver::where('employee_id', $id)->first();        
        $anyTeleSession = TelegramSession::where('user_id', $user->id)->first();

        $modelToSoftDelete = array();

       /**
        * check if user related to any request
        */
       if(UserRequest::where('employee_id', $id)->first() != null) {
            $modelToSoftDelete = $this->addSoftDeleteUserEmployee($modelToSoftDelete);
        }

        /**
         * check if employee is driver
         */
        if(isset($driver)){

            $modelToSoftDelete = $this->addSoftDeleteUserEmployee($modelToSoftDelete);
            
            $driver->updated_by = $currentUserId;

            if($driver->car_id != NULL) {
                //release assigned car to employee is driver
                $carAssigned = Vehicle::where('id', $driver->car_id)->first();
                $carAssigned->driver_id = NULL;
                $carAssigned->updated_by = $currentUserId;
                $carAssigned->save();

                $driver->car_id = NULL;
            }

            $driver->save();
            array_push($modelToSoftDelete, self::DRIVER_MODEL_NAME);
        }

        /**
         * Check if user registered in telegram session
         */
        if(isset($anyTeleSession)) {
            $modelToSoftDelete = $this->addSoftDeleteUserEmployee($modelToSoftDelete);
            array_push($modelToSoftDelete, self::TELEGRAM_SESSION_MODEL_NAME);
        }

        if(count($modelToSoftDelete) < 1) {
            $employee->forceDelete();
            $user->forceDelete();
            if(isset($driver)){
                $driver->forceDelete();
            }

            if(isset($anyTeleSession)) {
                $anyTeleSession->forceDelete();
            }

            return redirect('/master/pegawai')->with('status', 'Data pegawai berhasil dihapus!'); 
        }

        foreach($modelToSoftDelete as $model) {
            switch($model){
                case self::EMPLOYEE_MODEL_NAME :
                    $employee->delete();
                    break;
                case self::USER_MODEL_NAME:
                    $user->delete();
                    break;
                case self::DRIVER_MODEL_NAME:
                    $driver->delete();
                    break;
                case self::TELEGRAM_SESSION_MODEL_NAME:
                    $anyTeleSession->delete();
                    break;
            }
        }

        return redirect('/master/pegawai')->with('status', 'Data pegawai berhasil dihapus!');
    }

    private function addSoftDeleteUserEmployee($array) {
        if(!in_array(self::EMPLOYEE_MODEL_NAME, $array)) {
            array_push($array, self::EMPLOYEE_MODEL_NAME);
        }

        if(!in_array(self::USER_MODEL_NAME, $array)) {
            array_push($array, self::USER_MODEL_NAME);
        }

        return $array;
    }

    private function tryRestoreEmployee($request) {
        
        $anyEmployee = Employee::withTrashed()->where('email', $request->email)->first();
        $any = "email";
        if(!isset($anyEmployee)) {
            $anyEmployee = Employee::withTrashed()->where('nik', $request->nik)->first();
            $any = "nik";
        }

        if(!isset($anyEmployee)) {
            $anyEmployee = Employee::withTrashed()->where('mphone_number', $request->mphone_number)->first();
            $any = "nomor telepon";
        }

        if(!isset($anyEmployee)) {      
            $anyEmployee = Employee::withTrashed()->where('messaging_id', $request->messaging_id)->first();
            $any = "username telegram";
        }
        
        
        if($anyEmployee && $anyEmployee->deleted_at != null) {
            $anyEmployee->restore();
            $user = User::withTrashed()->where('id', $anyEmployee->user_id)->first();
            $user->restore();

            if($anyEmployee->title_id == 3) {
                $driver = Driver::withTrashed()->where('employee_id', $anyEmployee->id)->first();
                $driver->restore();
                $driver->status_id = 7;
                $driver->availability_id = 1;
                $driver->save();
            }

            return $any;
        } 

        return null;

    }

}
