<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;

use App\Models\VehicleStatus;
use App\Models\Vehicle;
use App\Models\Driver;
use App\Models\Employee;
use App\Models\Booking;
use App\Http\Requests\VehicleRequest;

class VehicleController extends Controller {

    public function __invoke()
    {
        $user_name = Auth::user()->name;

        $vehicles = Vehicle::with(['status'])->orderBy('id', 'asc')->simplePaginate(5);

        return view('layouts.admin.vehicle', ['user_fullname' => $user_name, 
                        'vehicles' => $vehicles, 'searchKey' => '']);
    }

    public function save(VehicleRequest $request) {
        
        $validated = $request->validated();
        $redirect = $this->validatePlatNumber($request);
        if($redirect != NULL) return $redirect;

        $userId = Auth::user()->id;

        $vehicle = new Vehicle;
        $vehicle->model_version = $validated['model_version'];
        $vehicle->brand = $validated['brand'];
        $vehicle->plat_number = $validated['plat_number'];
        $vehicle->km = $validated['km'];
        $vehicle->status_id = 1;// available
        $vehicle->updated_by = $userId;
        $vehicle->created_by = $userId;

        $vehicle->save();

        return redirect('/master/kendaraan')->with('status', 'Kendaraan berhasil ditambahkan!');
    }

    public function update(VehicleRequest $request) {
        $validated = $request->validated();
        $userId = Auth::user()->id;

        $savedVehicle = Vehicle::find($request->id);

        if($savedVehicle->plat_number != $request->plat_number){
            $redirect = $this->validatePlatNumber($request);

            if($redirect != NULL) return $redirect;
        }

        $savedVehicle->model_version = $validated['model_version'];
        $savedVehicle->brand = $validated['brand'];
        $savedVehicle->km = $validated['km'];
        $savedVehicle->plat_number = $validated['plat_number'];
        $savedVehicle->updated_by = $userId;

        $savedVehicle->save();

        return redirect('master/kendaraan')->with('status', 'Kendaraan berhasil diubah!');
    }

    public function delete($id) {
        $vehicle = Vehicle::where('id',$id)->first();
        $currentUserId = Auth::user()->id;

        if($vehicle->status_id == 2) { //there is request on process that booked this car
            return redirect('master/kendaraan')->withErrors(['Kendaraan tidak dapat dihapus, ada pengajuan yang masih menggunakan']);
        }

        $driverAssigned = Driver::where('car_id', $id)->first();
        if($driverAssigned != null) { 

            $request = Booking::where('car_id', $vehicle->id)->first();
            if($vehicle->driver_id != NULL) { // if this car assigned to a driver
                $this->releaseVehicleDriverRelation($vehicle, $currentUserId);
            }
       
            if($request != null)  {//if any request with this car
                $vehicle->delete();
            } else {
                $vehicle->forceDelete();
            }

            return redirect('master/kendaraan')->with('status', 'Kendaraan berhasil di hapus');
        }
        else if($vehicle->driver_id != NULL) {
            $driverEmployeeId = Driver::find($vehicle->driver_id)->employee_id;
            $employee = Employee::find($driverEmployeeId)->name;
            $this->releaseVehicleDriverRelation($vehicle, $currentUserId);
            $vehicle->forceDelete();
            return redirect('master/kendaraan')->with('status', 'Kendaraan berhasil dihapus! pengemudi '.$employee.' sekarang belum terassign kendaraan');
        }
        else {
            $vehicle->forceDelete();
            return redirect('master/kendaraan')->with('status', 'Data kendaraan berhasil dihapus!');
        }

        
    }

    public function search(Request $request) {
        if(Vehicle::all()->count() == 0)
            return redirect('/master/kendaraan')->withErrors(['Data kendaraan masih kosong!']);

       $search_query = $request->searchkey;
        
        $user_name = Auth::user()->id;
        $notification = ["Aghi", "Aghia"];

        $vehicles = Vehicle::with(['status'])
                        ->orderBy('id', 'asc')
                        ->where('plat_number','like','%'.$search_query.'%')
                        ->orWhere('brand', 'like', '%'.$search_query.'%')
                        ->orWhere('model_version', 'like', '%'.$search_query.'%')
                        ->simplePaginate();

        return view('admin.vehicle', ['user_fullname' => $user_name,
                        'vehicles' => $vehicles, 'searchKey' => $search_query]);
    }

    private function releaseVehicleDriverRelation($vehicle, $currentUserId) { 
        $driver = Driver::find($vehicle->driver_id);
          
        $driver->car_id = NULL;
        $driver->updated_by = $currentUserId;        
        $driver->save();

        $vehicle->driver_id = NULL;
        $vehicle->save();
    
    }

    private function validatePlatNumber($request) {
        $additionalValidator = Validator::make($request->all(), [
            'plat_number' => 'unique:cars'
        ], $messages = [
            'plat_number.unique' => 'Kendaraan dengan plat nomor yang diberikan sudah terdaftar'
        ]);
    
        if ($additionalValidator->fails()) {
            return redirect('master/kendaraan')
                    ->withErrors($additionalValidator)
                    ->withInput();
        }
    }
}