<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use WeStacks\TeleBot\TeleBot;
use WeStacks\TeleBot\Objects\KeyboardButton;
use WeStacks\TeleBot\Objects\Keyboard\ReplyKeyboardMarkup;

use Carbon\Carbon;

use App\Models\User;
use App\Models\UserRequest;
use App\Models\Employee;
use App\Models\Driver;
use App\Models\Vehicle;
use App\Models\DriverPresence;
use App\Models\TelegramSession;
use Illuminate\Support\Facades\Log;


class TelegramController extends Controller
{
    
    private $message_id;
    
    public function __invoke() {
    }

    public function processIncoming(Request $request) {
        $bot = new TeleBot(env('TELEGRAM_BOT_TOKEN'));
        if(isset($request->message) && $request->message['message_id'] != $this->message_id) {
            $this->message_id = $request->message['message_id'];
            $chat_id = $request->message['chat']['id'];
            $message = $request->message['text'];

            $username_exist = array_key_exists('username', $request->message['from']);

            if(!$username_exist) {

                $bot->sendMessage([
                    'chat_id' => $chat_id,
                    'text' => "Halo selamat datang di Simoka,". 
                    "\nTelegram Anda belum memiliki username,".
                    "\nSilahkan di set terlebih dahulu ya.".
                    "\n\nUsername bisa diset dengan klik foto profil Anda, dibagian Account isi username Anda.".
                    "\n\nJika sudah, silahkan chat lagi /start".
                    "\nTerima kasih"
                ]);
                return;
            }

            $username = $request->message['from']['username'];
            $employee = Employee::where('messaging_id', $username)->first();
            $user;
            
            if($employee != NULL){
                $user = User::find($employee->user_id);
            } else {
                $bot->sendMessage([
                    'chat_id' => $chat_id,
                    'text' => "Halo ".$username.", Apakabar?".
                    "\n sepertinya telegram Anda belum terdaftar di Simoka,".
                    "\natau Anda belum terdaftar sebagai pegawai coba hubungi admin ya :)"
                ]);
                return;
            }

            if($user == NULL) {
                $bot->sendMessage([
                    'chat_id' => $chat_id,
                    'text' => "Halo ".$username.", apakabar?".
                    "\nsepertinya telegram Anda belum terdaftar di Simoka,".
                    "\natau belum terdaftar sebagai pegawai coba hubungi admin ya :)"
                ]);

                return;
            }

            $anyTele = TelegramSession::where('chat_id', $chat_id)->first();

            if($user != NULL && $anyTele == NULL) { 
                $anyTele = new TelegramSession;
                $anyTele->user_id = $user->id;
                $anyTele->username = $username;
                $anyTele->chat_id = $chat_id;
                $anyTele->save();
            }
            
            if($anyTele != NULL && $anyTele->session != NULL && $anyTele->step == 1 && $employee->title_id == 3) {
              
                $lastSession = Carbon::parse($anyTele->session)->diffInHours(Carbon::now());

                if($lastSession > 1) {
                    $bot->sendMessage([
                        'chat_id' => $chat_id,
                        'text' => "Sesi absen sebelumnya sudah habis, silahkan chat /absen lagi ya"
                    ]);

                    $anyTele->session = null;
                    $anyTele->step = 0;
                    $anyTele->save();
                    return;
                }

                $this->replyUpdateStatus($chat_id, $message, $employee->id, $anyTele);
                return;

            } else if($message === '/start') {

                $msg;$keyboard;

                if($employee->title_id == 3) {

                    $msg = "Halo ".$employee->name." selamat datang di Simoka :)".
                    "\n\nAnda bisa melakukan absen tiap harinya dengan chat /absen".
                    "\n\nSelamat Bekerja :)";

                } else if(!isset($user->login_first_time)) {
                    $msg = "Halo ".$employee->name." selamat datang di Simoka :)".
                    "\nSaat ini, akun Anda sudah terverifikasi".
                    "\n\nSilahkan login ke website simoka dengan:".
                    "\n Email: ".$user->email.
                    "\n Password: Simoka21".
                    "\nSetelah pertama kali login, Anda bisa mengganti password.".
                    "\n\nTerima kasih, Selamat bekerja :)";

                    $url = env('APP_URL').'login';

                    $keyboard = [
                        'inline_keyboard' => [
                            [
                                ['text' => 'Login Simoka', 'url' => $url]
                            ]
                        ]
                    ];
                   
                } else {
                    $msg = "Halo ".$employee->name.", Selamat datang di Simoka :)".
                            "\nMelalui SimokappBot Anda akan mendapat notifikasi terkait peminjaman kendaraan operasional yang Anda ajukan.";
                }

                if(isset($keyboard)){
                    $bot->sendMessage([
                        'chat_id' => $chat_id,
                        'text' => $msg,
                        'reply_markup' => $keyboard
                    ]);
                    return;
                }

                $bot->sendMessage([
                    'chat_id' => $chat_id,
                    'text' => $msg
                ]);
                return;

            } else if($message === '/absen') {

                if($employee->title_id == 3) { //if jabatan or title id is driver
                    $this->askDriverStatus($username, $chat_id, $employee->id, $anyTele);
                    return;

                } else {
                    $this->replyOnlyForDriver($chat_id);
                    return;
                }

            } else if(str_contains($message, "simoka")){
                $message = $bot->sendMessage([
                    'chat_id' => $chat_id,
                    'text' => "Iya ".$employee->name
                ]);
                return;
            } 
            else if(str_contains($message, "ok")){
                $message = $bot->sendMessage([
                    'chat_id' => $chat_id,
                    'text' => "Ashiaap"
                ]);
                return;
            } 
            else if(str_contains($message, "makasih")) {
                $message = $bot->sendMessage([
                    'chat_id' => $chat_id,
                    'text' => "Sama - sama :)"
                ]);
                return;
            }
            else {
                $message = $bot->sendMessage([
                    'chat_id' => $chat_id,
                    'text' => "Halo ".$employee->name." Selamat Bekerja :)"
                ]);
                return;
            }
        }
    }

    private function replyOnlyForDriver($chat_id) {
        $bot = new TeleBot(env('TELEGRAM_BOT_TOKEN'));
        $message = $bot->sendMessage([
            'chat_id' => $chat_id,
            'text' => "Mohon maaf, perintah /absen hanya untuk pegawai dengan jabatan driver"
        ]);
    }

    private function askDriverStatus($name, $chat_id, $employee_id, $anyTele) {
        $bot = new TeleBot(env('TELEGRAM_BOT_TOKEN'));
        $driver = Driver::where('employee_id', $employee_id)->first();
        $employee = Employee::select('name')->where('id', $employee_id)->first();

        if($driver == NULL) {
            $message = $bot->sendMessage([
                'chat_id' => $chat_id,
                'text' => "Mohon maaf, sepertinya data Anda kurang lengkap di sistem, silahkan"
            ]);

            return;
        }
        
        $presence = DriverPresence::where('driver_id', $driver->id)
                    ->whereDate('created_at',Carbon::today())
                    ->first();

        if($presence != null) {
            $message = $bot->sendMessage([
            'chat_id' => $chat_id,
            'text' => "Anda sudah absen hari ini :)".
                      "\nTerima kasih"
            ]);

            return;
        }

        $anyTele->session = Carbon::now()->format('Y-m-d H:i:s');
        $anyTele->step = 1;
        $anyTele->save();
        
        $msg ="Halo ".$name.",".
        "\nApakah anda masuk kerja hari ini?".
        "\nSilahkan pilih nomor jawaban:".
        "\n1. Ya".
        "\n2. Izin".
        "\n3. Cuti".
        "\n4. Sakit";

        $message = $bot->sendMessage([
            'chat_id' => $chat_id,
            'text' => $msg
        ]);
    }

    private function replyUpdateStatus($chat_id, $status, $employee_id, $telsess) {
        $bot = new TeleBot(env('TELEGRAM_BOT_TOKEN'));
        $msg = "";
        $updateStatus= 0;
      
        switch($status) {
            case '1':
                $msg = "Terima kasih informasinya, selamat bekerja :)";
                $updateStatus = 6;
                break;
            case '2':
                $msg = "Terima kasih informasinya, hari ini status Anda sudah diupdate menjadi izin";
                $updateStatus = 3;
                break;
            case '3':
                $msg = "Terima kasih informasinya, hari ini status Anda sudah diupdate menjadi cuti";
                $updateStatus = 4;
                break;
            case '4':
                $msg = "Terima kasih informasinya, semoga lekas sembuh dan bisa kembali bekerja";
                $updateStatus = 5;
                break;
            case '/batal':
                $telsess->session = null;
                $telsess->step = 0;
                $telsess->save();
                $msg = "absen dibatalkan";
                break;
            default: $msg = "Anda masih dalam proses absensi,".
                            " silahkan dijawab berdasarkan nomor".
                            " atau kirim /batal untuk membatalkan absen";
                
        }

        if($updateStatus > 0) {
            $driver = Driver::where('employee_id', $employee_id)->first();

            if($updateStatus != 6){
                $request = UserRequest::where('driver_id', $driver->id)
                                ->where('travel_date', Carbon::today())
                                ->where('status_id', 2)
                                ->first();
                if($request != null) {
                    $request->driver_id = null;

                    //updateDriver additional value
                    $driver->availability_id = 1;
            
                    //update car
                    $car = Vehicle::where('id', $driver->car_id)->first();
                    $car->status_id = 1;
                    $car->updated_by = 1;
                    $car->save();

                    $msg_to_admin = "Driver ".$driver->name." hari ini tidak masuk kerja,".
                                    "\nPengajuan dengan nomor ".$request->request_number." perlu di assign driver lain";
                    $this->sendTelegramMessageToAdmin($msg_to_admin);
                }
            }
            
            $driver->status_id = $updateStatus;
            $driver->updated_by = 1;
            $driver->save();

            $absen = new DriverPresence;
            $absen->driver_id = $driver->id;
            $absen->status_id = $updateStatus;
            $absen->save();

            $telsess->session = null;
            $telsess->step = 0;
            $telsess->save();
        }

        $message = $bot->sendMessage([
            'chat_id' => $chat_id,
            'text' => $msg
        ]);

    }
 }