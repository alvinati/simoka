<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'nik' => 'required|max:255',
            'role_id' => 'required|int|min:1',
            'division' => 'required|int|min:1',
            'title' => 'required|int|min:1',
            'mphone_number' => 'required|numeric|digits_between:10,14|starts_with:08',
            'messaging_id' => 'required|string|max:50',
            'address' => 'required|string|max:255',
            'email' => 'required|max:255|email:rfc,dns'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Field nama tidak boleh kosong, isi sesuai dengan data karyawan',
            'nik.required' => 'Field NIK tidak boleh kosong, isi sesuai dengan data karyawan',
            'division.required' => 'Field divisi tidak boleh kosong, isi sesuai dengan data karyawan',
            'division.min' => 'Divisi karyawan harus ditentukan, Anda dapat memilih berdasarkan dropdown',
            'title.required' => 'Field jabatan tidak boleh kosong, isi sesuai dengan data karyawan',
            'title.min' => 'Jabatan karyawan harus ditentukan, Anda dapat memilih berdasarkan dropdown',
            'role_id.required' => 'Field Role tidak boleh kosong, isi sesuai dengan data karyawan',
            'role_id.min' => 'Role user karyawan harus ditentukan, Anda dapat memilih berdasarkan dropdown',
            'mphone_number.required' => 'Field nomor HP tidak boleh kosong, isi dengan nomor HP aktif karyawan',
            'messaging_id.required' => 'Field username Telegram tidak boleh kosong, isi dengan username Telegram aktif karyawan',
            'address.required' => 'Field alamat tidak boleh kosong, isi sesuai dengan domisili karyawan saat ini',
            'email.required' => 'Field email tidak boleh kosong, isi dengan email kantor karyawan',
            'email.email' => 'Format email tidak valid, harap cek kembali',
            'mphone_number.digits_between' => 'Nomor HP minimal 10 karakter dan maksimal 14 karakter',
            'mphone_number.starts_with' => 'Nomor HP tidak valid, isi dengan awalan 08',
            'mphone_number.numeric' => 'Isi nomor HP dengan angka',
            'messaging_id.max'=> 'Username maksimal 50 karakter'
        ];
    }
}
