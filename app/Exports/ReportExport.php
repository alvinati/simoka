<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Models\Booking;
use App\Models\Requisite;
use App\Models\Employee;
use App\Models\Driver;
use Illuminate\Support\Collection;

use App\Utils\StringUtils;
use App\Utils\NumberUtils;

class ReportExport implements FromCollection, WithHeadings
{

    protected $month;
    protected $year;

    function __construct($month, $year) {
        $this->month = $month;
        $this->year = $year;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $bookings = Booking::with(['request'])
                            ->whereYear('created_at', '=', $this->year)
                            ->whereMonth('created_at', '=', $this->month)
                            ->where('status_id', 2) //2 means finished
                            ->orderBy('return_date', 'desc')->get();

        $reports = collect([]);
        if(count($bookings) > 0) {
            foreach($bookings as $key=>$b) {
              
                $requisite = Requisite::select('requisite')->where('id', $b->request->requisite_id)->first();
                $employee = Employee::select('name')->where('id', $b->request->employee_id)->first();
                $driverEmployee = Driver::select('employee_id')->where('id', $b->request->driver_id)->first();
                $driver = Employee::select('name')->where('id', $driverEmployee->employee_id)->first();
                 
                $report = collect([
                    $b->request->request_number,
                    $employee->name,
                    $b->request->destination,
                    $driver->name,
                    StringUtils::toLocalDateString($b->exit_date),
                    StringUtils::toLocalDateString($b->return_date),
                    NumberUtils::toStringRupiah($b->total_cost)
                ]);

                $reports->push($report);
            };
        }

        return $reports;
    }

    public function headings() : array {
        return['Nomor Pengajuan', 'Nama Pegawai', 'Tujuan', 'Pengemudi', 'Tanggal Pergi', 'Tanggal Kembali', 'Total Biaya'];
    }
}
