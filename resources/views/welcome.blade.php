
@extends('layouts/base/authbase')
@section('auth-content')
<div class="auth-box row">

    <div class="col-lg-12 col-md-12">
        @if($errors->any())
            <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>Gagal!</strong>
                <p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </p>
            </div>
        @endif
    </div>
    
    <div class="col-lg-5 col-md-7 bg-white">
        <div class="p-3">
            <div class=" mt-3 text-center"><img width="170px" height="80px"src="{{asset('theme/images/Simoka.png')}}"/></div>
                <p class="text-center mt-3">Sistem Monitoring Kendaraan Operasional Kantor</p>
           
                <div class="row mb-4 text-center">
                    <div class="col-lg-12">
                        <div>
                            <label style="font-size:14px">Hanya butuh beberapa langkah untuk meminjam mobil kantor: </label>

                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div>
                            <h5 style="font-size:12px"><i class="fas fa-check text-primary"></i> Aktivasi akun dengan chat SimokappBot</h5>
                            <h5 style="font-size:12px"><i class="fas fa-check text-primary"></i> Isi Form Pengajuan</h5>
                            <h5 style="font-size:12px"><i class="fas fa-check text-primary"></i> Tunggu notifikasi Telegram</h5>
                        </div>
                    </div>
                    <div class="col-lg-12 text-center mt-4">
                        <a href="/login" class="btn btn-outline-primary btn-rounded">Login Sekarang</a>
                    </div>
                </div>
        </div>
    </div>
    <div class="col-lg-7 col-md-5 modal-bg-img" style="background-image: url({{asset('theme/images/landingpage/man-car.jpg')}});">
        
    </div>
</div>
@endsection