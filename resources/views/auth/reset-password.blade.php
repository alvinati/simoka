
@extends('layouts/base/authbase')
@section('auth-content')
<div class="auth-box row">
    <div class="col-lg-12 col-md-12 bg-white">
        <div class="p-3">
            <h2 class="mt-3 text-center">Ubah Kata Sandi</h2>
            @if(isset($message)) 
                <h5 class="text-center">{{$message}}</h5>
            @endif
            <form class="mt-4" method="POST" action="/ganti-password">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        @if($errors->any())
                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong>Gagal!</strong>
                            <p>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </p>
                        </div>
                        @endif

                        @if(session('warning'))
                        <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            {{ session('warning') }}
                        </div>
                        @endif
                    </div>
                    
                    @csrf
                    <input id="id" name="id" value="{{$id}}"hidden>
                    <input id="token" name="token" value="{{$token}}"hidden>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="text-dark" for="password">Kata Sandi</label>
                            <input class="form-control" id="password" type="password" name="password"
                                placeholder="Masukkan Kata Sandi Baru">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="text-dark" for="repeatPassword">Ulangi Kata Sandi</label>
                            <input class="form-control" id="repeatPassword" type="password" name="repeatPassword"
                                placeholder="Ulangi Kata Sandi yang telah Anda ketik">
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-md-3"></div>
                    <div class="col-lg-6 col-md-6 text-center">
                        <button type="submit" class="btn btn-outline-primary btn-rounded mt-4 mb-2">Ubah Kata Sandi</button>
                    </div>
                    <div class="col-lg-3 col-md-3"></div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection