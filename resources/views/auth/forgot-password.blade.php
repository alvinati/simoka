
@extends('layouts/base/authbase')
@section('auth-content')
<div class="auth-box row">
    <div class="col-lg-12 col-md-12 bg-white">
        <div class="p-3">
            <h2 class="mt-3 text-center">Lupa Password</h2>
            <p class="text-center">Masukkan Username Telegram Anda</p>
            <form class="mt-4" method="POST" action="/lupa-password">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        @if($errors->any())
                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong>Gagal!</strong>
                            <p>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </p>
                        </div>
                        @endif
                        @if(session('status'))
                            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{ session('status') }}
                            </div>
                        @endif
                    </div>
                    
                    @csrf
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input class="form-control" id="uname" type="uname" name="uname"
                                placeholder="Masukkan username Telegram">
                        </div>
                        <p class="text-center" style="font-size:12px">Tautan untuk mengganti password akan dikirim melalui telegram</p>
                    </div>
                    
                    <div class="col-lg-3 col-md-3"></div>
                    <div class="col-lg-6 col-md-6 text-center">
                        <button type="submit" class="btn btn-outline-primary btn-rounded">Kirim</button>
                    </div>
                    <div class="col-lg-3 col-md-3"></div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection