@extends('layouts/base/authbase')
@section('auth-content')
<div class="auth-box row">
    <div class="col-lg-7 col-md-5 modal-bg-img" style="background-image: url({{asset('theme/images/big/login-bg.jpg')}});">
        
    </div>
    <div class="col-lg-5 col-md-7 bg-white">
        <div class="p-3">
            <h2 class="mt-3 text-center">Login</h2>
            <p class="text-center">Masukkan Email dan Password Anda untuk login</p>
            <form class="mt-4" method="POST" action="/login">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                    @if($errors->any())
                    <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong>Gagal!</strong>
                        <p>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </p>
                    </div>
                    @endif
                    @if(session('status'))
                    <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        {{ session('status') }}
                    </div>
                    @endif
                    </div>
                    @csrf
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="text-dark" for="email">Email</label>
                            <input class="form-control" id="email" type="email" name="email"
                                placeholder="Masukkan email Anda">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="text-dark" for="password">Password</label>
                            <input class="form-control" id="password" type="password" name="password"
                                placeholder="Masukkan Password Anda">
                        </div>
                    </div>
                    <div class="col-lg-12 text-center">
                        <button type="submit" class="btn btn-outline-primary btn-rounded">Login</button>
                    </div>
                    <div class="col-lg-12 text-center mt-5">
                        Lupa Password Anda? <a href="/lupa-password" class="text-danger">Lupa Password</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection