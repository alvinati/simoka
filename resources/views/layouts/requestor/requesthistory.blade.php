@extends('layouts/base/navrequestor')

@section('page-title')
Riwayat Pengajuan
@endsection

@section('content')
<div class="row">
<div class="col-lg-12">
    <div class="card">
        <div class="card-body">
            
            <div class="table-responsive">
                <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                    
                    <div class="row">
                    <div class="col-sm-12">
                    <table id="zero_config" class="table table-striped table-bordered no-wrap dataTable" role="grid" aria-describedby="zero_config_info">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Nomor Pengajuan: Klik untuk mengurutkan data" style="width: 0px;">Nomor Pengajuan</th>
                                <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Tanggal Pengajuan: Klik untuk mengurutkan data" style="width: 0px;">Tanggal Pengajuan</th>
                                <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Keperluan: Klik untuk mengurutkan data" style="width: 0px;">Keperluan</th>
                                <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Tujuan: Klik untuk mengurutkan data" style="width: 0px;">Tujuan</th>
                                <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Tanggal Berangkat: Klik untuk mengurutkan data" style="width: 0px;">Tanggal Berangkat</th>
                                <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Jam Berangkat: activate to sort column ascending" style="width: 0px;">Jam Berangkat</th>
                                <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 0px;">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($requests as $r)
                                @if($loop->iteration % 2 == 0)									
                                <tr class="odd">
                                    <td tabindex="0"><a href="riwayat-pengajuan/detail/{{$r->id}}">{{ $r->request_number }}</a></td>
                                    <td>{{$r->created_at}}</td>
                                    <td>{{$r->requisite->requisite}}</td>
                                    <td>{{$r->destination}}</td>
                                    <td style="">{{$r->travel_date}}</td>
                                    <td style="">{{$r->travel_time}}</td>
                                    @if($r->status_id == 1)
                                    <td><span class="badge bg-success text-white">{{ $r->status->status }}</span></td>
                                    @elseif($r->status_id == 2)
                                    <td><span class="badge bg-warning text-white">{{ $r->status->status }}</span></td>
                                    @elseif($r->status_id == 3)
                                    <td><span class="badge bg-danger text-white">{{ $r->status->status }}</span></td>
                                    @else
                                    <td><span class="badge bg-secondary text-white">{{ $r->status->status }}</span></td>
                                        @endif
                                </tr>
                                @else
                                <tr class="odd">
                                    <td tabindex="0"><a href="riwayat-pengajuan/detail/{{$r->id}}">{{ $r->request_number }}</a></td>
                                    <td>{{$r->created_at}}</td>
                                    <td>{{$r->requisite->requisite}}</td>
                                    <td>{{$r->destination}}</td>
                                    <td style="">{{$r->travel_date}}</td>
                                    <td style="">{{$r->travel_time}}</td>
                                    @if($r->status_id == 1)
                                    <td><span class="badge bg-success text-white">{{ $r->status->status }}</span></td>
                                    @elseif($r->status_id == 2)
                                    <td><span class="badge bg-warning text-white">{{ $r->status->status }}</span></td>
                                    @elseif($r->status_id == 3)
                                    <td><span class="badge bg-danger text-white">{{ $r->status->status }}</span></td>
                                    @else
                                    <td><span class="badge bg-secondary text-white">{{ $r->status->status }}</span></td>
                                    @endif
                                </tr>
                                    @endif
                                @endforeach
                        </tbody>
                    </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('custom-script')
<script>

$(document).ready(function() {

var table = $('#zero_config').DataTable( {
    paging: false,
    info: false,
    order:[]
  });
});
</script>
@endsection