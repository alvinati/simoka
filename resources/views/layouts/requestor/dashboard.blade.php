@extends('layouts/base/navrequestor')

@section('page-title')
Selamat Datang, {{$user_fullname}}
@endsection

@section('subtitle')
<h5 style="color:#8392a5">{{$currentDate}}</h5>
@endsection

@section('content')
<div class="row">
<div class="col-lg-12">
    <div class="card">
        <div class="card-body">
            <h5 class="mb-3 text-center"><strong>Status Pengajuan Peminjaman Anda Hari ini</strong></h5>
            
            @if(count($requests) > 0)
            <div class="table-responsive">
                <table class="table">
                    <thead class="bg-primary text-white">
                        <tr>
                            <th>#</th>
                            <th>Nomor Pengajuan</th>
                            <th>Tanggal Berangkat</th>
                            <th>Tujuan</th>
                            <th>Status</th>
                            <th>Detail</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($requests as $key=>$r)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$r->request_number}}</td>
                            <td>{{$r->travel_date}}</td>
                            <td>{{$r->destination}}</td>
                            @if($r->status_id == 1)
                            <td><span class="badge bg-success text-white">{{ $r->status->status }}</span></td>
                            @elseif($r->status_id == 2)
                            <td><span class="badge bg-warning text-white">{{ $r->status->status }}</span></td>
                            @elseif($r->status_id == 3)
                            <td><span class="badge bg-danger text-white">{{ $r->status->status }}</span></td>
                            @else
                            <td><span class="badge bg-secondary text-white">{{ $r->status->status }}</span></td>
                            @endif
                            <td> 
                                <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Lihat Detail"  class="detail-icon" href="riwayat-pengajuan/detail/{{ $r->id }}">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text align-middle me-2 text-primary"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @else
            <div class="d-flex justify-content-center">
                <h3>Belum ada pengajuan<h3>
            </div>
            @endif
            <div class="row justify-content-center mt-4 mb-4">
                <button id="create-new-request" class="btn btn-rounded btn-outline-primary">Buat Pengajuan Baru</button>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('custom-script')
<script>
$(document).ready(function() {
    $('#create-new-request').on('click', function() {
        var url = "{{route('form.request')}}";
        window.location.href = url;
    });
});
</script>
@endsection