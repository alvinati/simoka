@extends('layouts/base/navrequestor')

@section('page-title')
Formulir Pengajuan
@endsection

@section('subtitle')
Isi semua data yang diperlukan untuk meminjam kendaraan
@endsection

@section('content')
<div class="row">
<div class="col-lg-12">
    <div class="card">
        <div class="card-body">
            <form method="post" action="/submit-form">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-md">
                        @csrf
                        <tbody>
                            <tr>
                                <th>Tanggal Berangkat</th>
                                <td><input  class="form-control" type="date" name="travel_date"/> </td>
                            </tr>
                            <tr>
                                <th>Jam Berangkat</th>
                                <td><input  class="form-control" type="time" name="travel_time"/></td>
                            </tr>
                            <tr>
                                <th>Tujuan</th>
                                <td><input  class="form-control" type="text" name="destination"/></td>
                            </tr>
                            <tr>
                                <th>Keperluan</th>
                                <td>
                                <select class="form-control form-select" aria-label="Pilih Keperluan" id="requisite" name="requisite">
                                    <option value="0" selected>Pilih Keperluan</option>
                                    @foreach($requisites as $r)
                                    <option value="{{ $r->id }}">{{ $r->requisite }}</option>
                                    @endforeach
                                </select>
                                </td>
                            </tr>
                            <tr>
                                <th>Jumlah Penumpang</th>
                                <td><input class="form-control" type="number" name="passenger" min="1" max="5"/></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="d-flex justify-content-end">
                <button class="btn btn-primary" type="submit">Kirim Pengajuan</button>
            </div>
            </form>
        </div>
    <div>
</div>
</div>
@endsection