@extends('layouts/base/navrequestor')

@section('page-title')
 Detail Pengajuan
@endsection

@section('subtitle')
Detail pengajuan peminjaman kendaraan
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
            <table class="table table-md mt-2 mb-5">
                <tbody>
                    <tr class="mb-2">
                        <th>Nomor Pengajuan</th>
                        <td>: {{$data->request_number}}</td>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr class="mb-2">
                        <th>Tanggal Pengajuan</th>
                        <td>: {{$data->created_at->locale('id')->isoFormat('dddd, D MMMM Y') }}</td>
                        <th>Tujuan</th>
                        <td>: {{ $data->destination }}</td>
                    </tr>
                    <tr>
                        <th>Nama Pegawai</th>
                        <td>: {{ $data->employee->name }}</td>
                        <th>Tanggal Berangkat</th>
                        <td>: {{ $data->travel_date }}</td>
                    </tr>
                    <tr>
                        <th>Keperluan</th>
                        <td>: {{ $data->requisite->requisite }}</td>
                        <th>Jam Berangkat</th>
                        <td>: {{ $data->travel_time }}</td>
                    </tr>
                    <tr>
                        <th>Jumlah Penumpang</th>
                        <td>: {{$data->total_passenger}} Orang</td>
                        <th>Status</th>
                        @if($data->status_id == 1)
                        <td>: <span class="badge bg-success text-white">{{ $data->status->status }}</span></td>
                        @elseif($data->status_id == 2)
                        <td>: <span class="badge bg-warning text-white">{{ $data->status->status }}</span></td>
                        @elseif($data->status_id == 3)
                        <td>: <span class="badge bg-danger text-white">{{ $data->status->status }}</span></td>
                        @else
                        <td>: <span class="badge bg-secondary text-white">{{ $data->status->status }}</span></td>
                        @endif
                    </tr>
                    @csrf
                    <input type="hidden" id="id" name="id" value="{{$data->id}}">
                    <tr>
                        @if($data->status_id == 3)
                        <th>Keterangan Penolakan</th>
                        <td class="text-danger">: {{ $data->notes }}</td>
                        @else
                        <th>Driver</th>
                        <td>: {{$data->driver_name}}</td>
                        @endif

                        @if($data->status_id > 1 && $data->status_id != 3)
                            <th>Nomor Peminjaman</th>
                            @if($data->status_id == 4)
                            <td>: <a href="/riwayat-pengajuan/detail-peminjaman/{{$data->booking_id}}">{{$data->booking_number}}</a></td>
                            @else
                            <td>: {{$data->booking_number}} </td>
                            @endif
                        @endif
                    </tr>
                </tbody>
             </table>
            </div>
        </div>
    </div>
</div>
@endsection
