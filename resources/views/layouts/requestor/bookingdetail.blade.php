@extends('layouts/base/navrequestor')

@section('page-title')
Detail Peminjaman
@endsection

@section('subtitle')
Berikut data lengkap peminjaman kendaraan
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- Image Modal  -->
                <div id="image-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none; padding-right: 17px;" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body text-center">
                                <img width="300px" height="500px" id="img-on-modal" src=""/>
                            </div>
                        </div><!-- /.modal-content -->
                    </div>
                </div>
                    <table class="table table-md mt-2">
                        <tbody>
                            <tr class="mb-2">
                                <th style="font-size:16px">Nomor. <strong class="text-primary">{{$data->book_number}}</strong></th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Tanggal Pengajuan</th>
                                <td>: {{ $data->request->created_at->locale('id')->isoFormat('dddd, D MMMM Y') }}</td>
                                <th>Tujuan</th>
                                <td>: {{ $data->request->destination }}</td>
                            </tr>
                            <tr>
                                <th>Nama Pegawai</th>
                                <td>: {{ $data->requestor }}</td>
                                <th>Tanggal Berangkat</th>
                                <td>: {{ $data->request->travel_date }}</td>
                            </tr>
                            <tr>
                                <th>Keperluan</th>
                                <td>: {{ $data->requisite }}</td>
                                <th>Jam Berangkat</th>
                                <td>: {{ $data->request->travel_time }}</td>
                            </tr>
                            <tr>
                                <th>Pengemudi</th>
                                <td>: {{$data->driver_name}}</td>
                                <th>Mobil</th>
                                <td>: {{$data->car}}</td>
                            </tr>
                            <tr>
                                <th>Kilometer Mobil</th>
                                <td>: {{$data->km}} km</td>
                                <th></th>
                                <td></td>
                            </tr>
                            <tr>
                                <th style="font-size:16px;">Biaya Perjalanan</th>
                                <td></td>
                                <th> </th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Biaya Bensin</th>
                                <td>: {{ $data->fuel_cost_string}}</td>
                                <th>Biaya Tol</th>
                                <td>: {{$data->toll_cost_string}}</td>
                            </tr>
                            <tr>
                                <th>Struk Pembelian Bensin</th>
                                <td>
                                </td>

                                @if(isset($data->toll_cost))
                                <th>Struk Pembayaran Toll</th>
                                <td>
                                </td>
                                @endif
                            </tr>
                            <tr>
                                <th>
                                @if(isset($data->fuel_receipt_paths))
                                    @foreach($data->fuel_receipt_paths as $p)
                                    <a class="image-receipt" data-toggle="modal" data-target="#image-modal" data-src="{{asset($p->image_path)}}"> <img class="mb-1 me-2" style="height:80px; width:60px;" src="{{asset($p->image_path)}}"/></a>
                                    @endforeach
                                @endif
                                </th>
                                <td></td>

                                @if(isset($data->toll_cost) && isset($data->toll_receipt_paths))
                                <th>@foreach($data->toll_receipt_paths as $p) 
                                <a class="image-receipt" data-toggle="modal" data-target="#image-modal" data-src="{{asset($p->image_path)}}"> <img class="mb-1 me-2" style="height:80px; width:60px;" src="{{asset($p->image_path)}}"/></a>
                                    @endforeach</th>
                                <td>
                                </td>
                                @endif
                            </tr>
                        </tbody>
                    </table>
                    <div class="row justify-content-md-center">
                        <div class="col-md-2"><strong>Total Biaya</strong></div>
                        <div class="col-md-3">
                            <span>: {{ $data->total_cost_string }}</span>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-script')
<script>
$(document).ready(function() {
    $('.image-receipt').click(function() {
        var source = $(this).data('src');
        $('#image-modal').on('shown.bs.modal', function() {
            document.getElementById("img-on-modal").src = source;
        });
    });
});
</script>
@endsection
