@extends('layouts/base/navrequestor')

@section('page-title')
Profil
@endsection

@section('subtitle')
Anda dapat mengubah profil Anda di sini.
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body" >
                <!--  Modal account form -->
                <div class="modal fade" id="change-password-modal" tabindex="-1" role="dialog"
                    aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="change-password-label">Ubah Kata Sandi</h4>
                                <button type="button" class="close" data-dismiss="modal"
                                    aria-hidden="true">×</button>
                            </div>
                            <form class="container-fluid" action="profil/password" method="post" id="change-password-form">
                                <div class="modal-body">
                                
                                    @csrf
                                    <input type="hidden" id="id" name="id">
                                    <div class="row mb-3">
                                        <div class="col-4">
                                            <label for="newPassword" class="col-form-label">Kata Sandi Baru </label>
                                        </div>
                                        <div class="col-8">
                                            <input type="password" class="form-control" id="newPassword" name="newPassword">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-4">
                                            <label for="repeatPassword" class="col-form-label">Ulangi Kata Sandi </label>
                                        </div>
                                        <div class="col-8">
                                            <input type="password" class="form-control" id="repeatPassword" name="repeatPassword">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light" data-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary btn-change-password-form">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row" style="padding:30px">
                   <img alt="user" width="120" class="rounded-circle" src="../theme/images/users/profile-pic.jpg">
                   <h2  style="margin-top:40px; margin-left:20px" class="">{{$user_fullname}}</h2>
                </div>
                <form method="post" action="/update-profil">
                    @csrf
                    <input type="hidden" id="id" name="id" value="{{$user->id}}">
                    <input type="hidden" id="role_id" name="role_id" value="{{$user->role_id}}">
                    <table class="table col-md-8 col-sm-12 col-lg-8">
                        <tbody>	
                            <tr>
                                <th>Nama</th>
                                <td><input class="form-control" type="text" name="name" value="{{$user->name}}"></td>
                            </tr>
                            <tr>
                                <th>NIK</th>
                                <td><input class="form-control" type="text" name="nik" value="{{$user->employee->nik}}"></td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td><input class="form-control" type="email" name="email" value="{{$user->email}}"></td>
                            </tr>
                            <tr>
                                <th>No HP</th>
                                <td><input class="form-control" type="tel" name="mphone_number" value="{{$user->employee->mphone_number}}"></td>
                            </tr>
                            <tr>
                                <th>Username Telegram</th>
                                <td><input class="form-control" type="tel" id="messaging_id" name="messaging_id" value="{{$user->employee->messaging_id}}"></td>
                            </tr>
                            <tr>
                                <th>Divisi</th>
                                <td>
                                    <select class="form-control form-select" aria-label="Pilih Divisi Pegawai" id="division" name="division">
                                    <option value="{{$user->employee->division_id}}" selected>{{$user->employee->division->name}}</option>
                                    @foreach($divisions as $d)
                                    <option value="{{ $d->id }}">{{ $d->name }}</option>
                                    @endforeach
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>Jabatan</th>
                                <td>
                                    <select class="form-control form-select" aria-label="Pilih Jabatan Pegawai" id="title" name="title">
                                    <option value="{{$user->employee->title_id}}" selected>{{$user->employee->title->name}}</option>
                                    @foreach($titles as $t)
                                    <option value="{{ $t->id }}">{{ $t->name }}</option>
                                    @endforeach
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>Alamat</th>
                                <td><textarea class="form-control"  name="address">{{$user->employee->address}}</textarea></td>
                            </tr>

                            <tr>
                                <th>Kata Sandi</th>
                                <td><input class="form-control" type="password" name="password" value="xxxxxxxx" disabled></td>
                                <td><a data-toggle="modal" data-target="#change-password-modal" class="btn btn-sm btn-primary text-white">Ubah Kata Sandi</a>
                            </tr>
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection