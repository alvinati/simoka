@extends('layouts/base/navadmin')

@section('page-title')
Data Pengemudi
@endsection

@section('subtitle')
Anda dapat mengubah data pengemudi disini, menghapus / menambahkan data pengemudi dapat dilakukan melalui tabel pegawai
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!--  Modal employee form -->
                <div class="modal fade" id="driver-form-modal" tabindex="-1" role="dialog"
                    aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="driver-form-label">Data Pengemudi</h4>
                                <button type="button" class="close" data-dismiss="modal"
                                    aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                            <form class="container-fluid" action="" method="post" id="driver-form">
                                @csrf
                                <input type="hidden" id="id" name="id">
                                <div class="row mb-3">
                                    <div class="col-3">
                                    <label for="name" class="col-form-label">Nama</label>
                                    </div>
                                    <div class="col-9">
                                    <input type="text" class="form-control" id="name" name="name">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-3">
                                    <label for="mphone_number" class="col-form-label">No HP</label>
                                    </div>
                                    <div class="col-9">
                                    <input class="form-control" type="tel" id="mphone_number" name="mphone_number"></input>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-3">
                                    <label for="address" class="col-form-label">Alamat</label>
                                    </div>
                                    <div class="col-9">
                                    <input class="form-control" type="text" id="address" name="address"></input>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-3">
                                    <label for="car_id" class="col-form-label">Mobil Dinas</label>
                                    </div>
                                    <div class="col-4">
                                    <select class="form-control form-select" aria-label="Pilih Mobil Dinas" id="car_id" name="car_id">
                                        <option value="0" selected>Pilih Mobil</option>
                                        @foreach($cars as $c)
                                        <option value="{{ $c->id }}">{{ $c->plat_number }}</option>
                                        @endforeach
                                    </select>
                                    </div>
                                </div>
                                @if(count($cars) < 1)
                                <div class="row mb-3">
                                <div class="col-3"></div>
                                <div class="col-4">
                                <a class="btn btn-sm btn-success mt-2 btn-to-vehicle text-white">cek kendaraan <i class="fas fa-share-square"></i></a>
                                </div>
                                </div>
                                @endif
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary btn-driver-form">Simpan</button>
                                </div>
                            </form>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <!-- /.tabel-content -->
                <div class="table-responsive">
                    <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                        <div class="row">
                        <div class="col-sm-12">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap dataTable" role="grid" aria-describedby="zero_config_info">
                            <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Nama: Klik untuk mengurutkan data" style="width: 0px;">Nama</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="No HP: Klik untuk mengurutkan data" style="width: 0px;">No HP</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Alamat: Klik untuk mengurutkan data" style="width: 0px;">Alamat</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Mobil Dinas: Klik untuk mengurutkan data" style="width: 0px;">Mobil Dinas</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Status Kehadiran: Klik untuk mengurutkan data" style="width: 0px;">Status Kehadiran</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Aksi: activate to sort column ascending" style="width: 0px;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>	
                            @foreach($drivers as $d)
                                @if($loop->iteration % 2 == 0)									
                            <tr class="odd">
                                <td class="" tabindex="0">{{ $d->employee->name}}</td>
                                <td style="">{{ $d->employee->mphone_number }}</td>
                                <td style="">{{ $d->employee->address }}</td>
                                @if($d->car != NULL)
                                <td style="">{{ $d->car->plat_number }}</td>
                                @else
                                <td style=""></td>
                                @endif
                                
                                @if($d->status_id == 1 || $d->status_id == 6)
                                <td style=""><span class="badge bg-success text-white">{{ $d->status->status }}</span></td>
                                @elseif($d->status_id == 2)
                                <td style=""><span class="badge bg-warning text-white">{{ $d->status->status }}</span></td>
                                @else 
                                <td style=""><span class="badge bg-danger text-white">{{ $d->status->status }}</span></td>
                                @endif

                                <td class="table-action">
                                    @if(isset($d->deleted_at))
                                    <a href="#" class="btn btn-secondary btn-sm disabled">Tidak Aktif</a>
                                    @else
                                    <a class="edit-icon" href="#" data-toggle="modal" data-target="#driver-form-modal" 
                                        data-drv='{ "id": "{{ $d->id }}", 
                                                    "name": "{{ $d->employee->name }}",
                                                    "mphone_number": "{{ $d->employee->mphone_number }}",
                                                    "address": "{{ $d->employee->address}}",
                                                    <?php 
                                                    if($d->car != NULL || $d->car_id != 0)
                                                    echo '"plat": "'. $d->car->plat_number .'",';
                                                    ?>
                                                    "status": "{{ $d->status->id }}"
                                                    }'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 align-middle">
                                        <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                        </svg>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                            @else
                            <tr class="even">
                                <td class="" tabindex="0">{{ $d->employee->name}}</td>
                                <td style="">{{ $d->employee->mphone_number }}</td>
                                <td style="">{{ $d->employee->address }}</td>
                                @if($d->car != NULL)
                                <td style="">{{ $d->car->plat_number }}</td>
                                @else
                                <td style=""></td>
                                @endif

                                @if($d->status_id == 1 || $d->status_id == 6)
                                <td style=""><span class="badge bg-success text-white">{{ $d->status->status }}</span></td>
                                @elseif($d->status_id == 2)
                                <td style=""><span class="badge bg-warning text-white">{{ $d->status->status }}</span></td>
                                @else 
                                <td style=""><span class="badge bg-danger text-white">{{ $d->status->status }}</span></td>
                                @endif
                                
                                <td class="table-action">
                                @if(isset($d->deleted_at))
                                <a href="#" class="btn btn-secondary btn-sm disabled">Tidak Aktif</a>
                                @else
                                <a class="edit-icon" href="#" data-toggle="modal" data-target="#driver-form-modal" 
                                    data-drv='{ "id": "{{ $d->id }}", 
                                                "name": "{{ $d->employee->name }}",
                                                "mphone_number": "{{ $d->employee->mphone_number }}",
                                                "address": "{{ $d->employee->address}}",
                                                <?php 
                                                if($d->car != NULL || $d->car_id != 0)
                                                echo '"plat": "'. $d->car->plat_number .'",';
                                                ?>
                                                "status": "{{ $d->status->id }}"
                                                }'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 align-middle">
                                    <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                    </svg>
                                </a>
                                @endif
                                </td>
                            </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-6">
                                <div class="dataTables_paginate paging_simple_numbers" id="zero_config_paginate">
                                    <ul class="pagination mt-3">
                                    {{ $drivers->links() }}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-script')

<script>

$(document).ready(function() {

 var table = $('#zero_config').DataTable( {
    paging: false,
    info: false
  });
  

 $(".btn-to-vehicle").click(function() {
    var url = "{{route('kendaraan')}}"
    window.location.href = url;
  });

  $(".edit-icon").click(function() {
    var drv = $(this).data('drv');

     $('#driver-form-modal').on('shown.bs.modal', function() {
        var modal = $(this);
        modal.find('#id').val(drv.id);
        modal.find('#name').val(drv.name);
        modal.find('#mphone_number').val(drv.mphone_number);
        modal.find('#address').val(drv.address);

        if(drv.plat != null ) {
          document.getElementsByName('car_id')[0].options[0].innerHTML = drv.plat;
        } else {
          document.getElementsByName('car_id')[0].options[0].innerHTML = "Pilih Mobil";
        }

        modal.find('#driver-form').attr("action", "{{ route('pengemudi.update') }}");
    });
  });
});

</script>

@endsection
