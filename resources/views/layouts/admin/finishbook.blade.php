@extends('layouts/base/navadmin')

@section('page-title')
@if($data->status_id==1)
Pengembalian Kendaraan
@else
Detail Peminjaman
@endif
@endsection

@section('subtitle')
@if($data->status_id==1)
Harap masukkan semua data yang diperlukan
@else
Berikut data lengkap peminjaman kendaraan
@endif
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            <!-- Finish Book Modal -->
            <div id="finish-book-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none; padding-right: 17px;" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Selesaikan Peminjaman</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <h6>Apa Anda yakin?</h6>
                            <hr>
                            <p style="font-size:12px;" class="mt-3">Catatan: </p>
                            <ul class="ms-3">
                            <li style="font-size:12px;">Peminjaman yang sudah diselesaikan tidak dapat diubah lagi</li>
                            </ul>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                            <button type="button" onclick="saveConfirm('form-finish-book')" class="btn btn-danger m-save-btn">Yakin</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div>
            </div>
            <!-- Image Modal  -->
            <div id="image-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none; padding-right: 17px;" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body text-center">
                            <img width="300px" height="500px" id="img-on-modal" src=""/>
                        </div>
                    </div><!-- /.modal-content -->
                </div>
            </div>

                <form method="post" action="/peminjaman/finish" enctype="multipart/form-data" id="form-finish-book">
                    <table class="table table-md mt-2">
                        <tbody>
                            <tr class="mb-2">
                                <th style="font-size:16px" class="text-primary"><strong>No: {{$data->book_number}}</strong></th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Tanggal Pengajuan</th>
                                <td>: {{ $data->request->created_at->locale('id')->isoFormat('dddd, D MMMM Y') }}</td>
                                <th>Tujuan</th>
                                <td>: {{ $data->request->destination }}</td>
                            </tr>
                            <tr>
                                <th>Nama Pegawai</th>
                                <td>: {{ $data->requestor }}</td>
                                <th>Tanggal Berangkat</th>
                                <td>: {{ $data->request->travel_date }}</td>
                            </tr>
                            <tr>
                                <th>Keperluan</th>
                                <td>: {{ $data->requisite }}</td>
                                <th>Jam Berangkat</th>
                                <td>: {{ $data->request->travel_time }}</td>
                            </tr>
                            <tr>
                                <th>Pengemudi</th>
                                <td>: {{$data->driver_name}}</td>
                                <th>Mobil</th>
                                <td>: {{$data->car}}</td>
                            </tr>
                            <tr>
                                <th>Kilometer Mobil</th>
                                @if($data->status_id == 1)
                                <td><input class="form-control" min="0" type="number" id="km" value="{{$data->km}}" name="km"></td>
                                @else
                                <td>: {{$data->km}} km</td>
                                @endif
                                <th></th>
                                <td></td>
                            </tr>
                            <tr>
                                <th style="font-size:16px" class="text-primary">Biaya Perjalanan</th>
                                <td></td>
                                <th></th>
                                <td></td>
                            </tr>
                            @csrf
                            <input type="hidden" id="id" name="id" value="{{$data->id}}">
                            <tr>
                                @if($data->status_id==1)
                                <th>Biaya Bensin</th>
                                <td><input  class="form-control" min="0" type="number" id="fuel" name="fuel" onchange="updateTotal()"></input></td>
                                <th>Biaya Tol</th>
                                <td><input class="form-control" min="0" type="number" id="toll" name="toll" onchange="updateTotal()"></input></td>
                                @else
                                <th>Biaya Bensin</th>
                                <td>: {{$data->fuel_cost_string}}</td>
                                @if(isset($data->toll_cost))
                                <th>Biaya Tol</th>
                                <td>: {{$data->toll_cost_string}}</td>
                                @endif
                                @endif
                            </tr>

                            @if($data->status_id==1)
                            <tr>
                                <th>Upload Struk bensin
                                    <label class="text-muted" style="font-size:10px;">*maksimal 2 MB/foto</label>
                                </th>
                                <td>
                                    <input type="file" class="form-control" id="fuel_receipts" name="fuel_receipts[]" onchange="readURL(this, '#fuel')" multiple hidden/>
                                    <label class="btn btn-primary btn-fuel" for="fuel_receipts">Upload</label>
                                </td>
                                <th>Upload Struk toll
                                    <label class="text-muted"style="font-size:10px;">*maksimal 2 MB/foto</label>
                                </th>
                                <td>
                                    <input type="file" class="form-control" id="toll_receipts" name="toll_receipts[]" onchange="readURL(this, '#toll')" multiple hidden/>
                                    <label class="btn btn-primary" for="toll_receipts">Upload</label>
                                </td>
                            </tr>

                            <tr>
                            <th>
                                @for($i=1; $i<=3 ; $i++)
                                <img class="mb-1 me-1" id="fuel-{{$i}}" style="height:80px; width:60px;" src="{{asset('theme/images/no_image.jpg')}}" alt="" title=""/>
                                @endfor                     
                            </th>
                            <td>
                            </td>

                            <th>
                                @for($i=1; $i<=3 ; $i++)
                                <img class="mb-1 me-1" id="toll-{{$i}}" style="height:80px; width:60px;" src="{{asset('theme/images/no_image.jpg')}}" alt="" title=""/>
                                @endfor                            
                            </th>
                            <td>
                            </td>
                            </tr>
                            @else
                                <tr>
                                    <th>Struk Pembelian Bensin</th>
                                    <td>
                                    </td>

                                    @if(isset($data->toll_cost))
                                    <th>Struk Pembayaran Toll</th>
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <th>
                                    @if(isset($data->fuel_receipt_paths))
                                        @foreach($data->fuel_receipt_paths as $p)
                                           <a class="image-receipt" data-toggle="modal" data-target="#image-modal" data-src="{{asset($p->image_path)}}"> <img class="mb-1 me-2" style="height:80px; width:60px;" src="{{asset($p->image_path)}}"/></a>
                                        @endforeach
                                    @endif
                                    </th>
                                    <td></td>

                                    @if(isset($data->toll_cost) && isset($data->toll_receipt_paths))
                                    <th>@foreach($data->toll_receipt_paths as $p) 
                                        <a class="image-receipt" data-toggle="modal" data-target="#image-modal" data-src="{{asset($p->image_path)}}"> <img class="mb-1 me-2" style="height:80px; width:60px;" src="{{asset($p->image_path)}}"/></a>
                                        @endforeach</th>
                                    <td>
                                    </td>
                                    @endif
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="row justify-content-md-center mb-5">
                        <div class="col-md-2"><strong>Total Biaya</strong></div>

                        @if($data->status->id == 1)
                        <div class="col-md-3">
                            <input class="form-control" type="number" id="total" name="total" hidden/>
                            <div id="total-cost">: Rp. 0,00</div>
                        </div>
                        @else 
                        <div class="col-md-3">
                            <span>: {{ $data->total_cost_string }}</span>
                        </div>
                        @endif
                    </div>
                    @if($data->status->id == 1)
                    <div class="d-flex flex-row-reverse mt-5">
                        <a href="/peminjaman" class="btn btn-secondary">Batal </a>
                        <a data-toggle="modal" style="margin-right:10px" data-target="#finish-book-modal" class="btn btn-primary text-white">Selesai</a>
                    </div>
                    @endif
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-script')
<script>

$(document).ready(function() {
    $('.image-receipt').click(function() {
        var source = $(this).data('src');
        $('#image-modal').on('shown.bs.modal', function() {
            document.getElementById("img-on-modal").src = source;
        });
    });
});

function saveConfirm(formId) {
    document.getElementById(formId).submit();   
}

function readURL(input, imageId) {

  if (input.files && input.files.length > 0) {
    for(var i=0; i < 3; i++) {
        var reader = new FileReader();
        var idNum = i+1;
        var id = imageId+"-" + idNum;

        reader.onload = assignImageSource(id);

        if(input.files[i]){
            reader.readAsDataURL(input.files[i]); // convert to base64 string
        } else {
            $(id).attr('src', "{{ asset('theme/images/no_image.jpg') }}");
        }
    }
  }
}

function assignImageSource(id) {
    return function(e) {
        $(id).attr('src', e.target.result);
    }
}

function updateTotal() {
    var fuel = document.getElementById("fuel").value;
    var toll = document.getElementById("toll").value;

    if(fuel == null || fuel == ""){
        fuel = "0";
    }

    if(toll == null || toll == "") {
        toll = "0";
    }

    var cost = parseFloat(fuel) + parseFloat(toll);

    var formatter = new Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR',
        // These options are needed to round to whole numbers if that's what you want.
        minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
        maximumFractionDigits: 2, // (causes 2500.99 to be printed as $2,501)
    });

    var formatted = formatter.format(cost);
    document.getElementById("total").value = cost;
    document.getElementById("total-cost").innerHTML = formatted;
}
</script>
@endsection