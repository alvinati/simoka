@extends('layouts/base/navadmin')

@section('page-title')
Data Kendaraan
@endsection

@section('subtitle')
Anda dapat mengatur data kendaraan disini
@endsection

@section('right-action-button')
<button  type="button" class="btn btn-sm btn-outline-primary btn-rounded btn-add-new" data-toggle="modal" data-target="#vehicle-form-modal"><i class="fas fa-plus"></i> Tambah Kendaraan</button>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
              
                <!--  Modal vehicle form -->
                <div class="modal fade" id="vehicle-form-modal" tabindex="-1" role="dialog"
                    aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="vehicle-form-label">Large modal</h4>
                                <button type="button" class="close" data-dismiss="modal"
                                    aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                            <form class="container-fluid" action="" method="post" id="vehicle-form">
                                @csrf
                                <input type="hidden" id="id" name="id">
                                <div class="row mb-3">
                                    <div class="col-3">
                                    <label for="plat_number" class="col-form-label">Plat Nomor </label>
                                    </div>
                                    <div class="col-9">
                                    <input type="text" class="form-control" id="plat_number" name="plat_number">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                <div class="col-3">
                                    <label for="brand" class="col-form-label">Merek Mobil</label>
                                    </div>
                                    <div class="col-9">
                                    <input class="form-control" id="brand" name="brand"></input>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-3">
                                    <label for="model_version" class="col-form-label">Model Mobil</label>
                                    </div>
                                    <div class="col-9">
                                    <input class="form-control" type="text" id="model_version" name="model_version"></input>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-3">
                                    <label for="km" class="col-form-label">Jarak Ditempuh (km)</label>
                                    </div>
                                    <div class="col-9">
                                    <input class="form-control" type="number" id="km" name="km"></input>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary btn-vehicle-form">Simpan</button>
                                </div>
                            </form>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <!--  Modal delete data -->
                <div id="delete-row" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none; padding-right: 17px;" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Hapus Data Kendaraan</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <h6>Apa Anda yakin untuk menghapus kendaraan?</h6>
                                <hr>
                                <p style="font-size:12px;" class="mt-3">Catatan: </p>
                                <ul class="ms-3">
                                <li style="font-size:12px;">Kendaraan yang terkait dengan data peminjaman hanya bisa dinonaktifkan</li>
                                </ul>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Batal</button>
                                <button type="button" class="btn btn-danger m-delete-btn">Hapus</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>

                <!-- /.tabel-content -->
                <div class="table-responsive">
                    <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                       
                        <div class="row">
                        <div class="col-sm-12">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap dataTable" role="grid" aria-describedby="zero_config_info">
                            <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Nama: Klik untuk mengurutkan data" style="width: 0px;">Plat Nomor</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Merek Mobil: Klik untuk mengurutkan data" style="width: 0px;">Merek Mobil</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Model Mobil: Klik untuk mengurutkan data" style="width: 0px;">Model Mobil</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Jarak Tempuh: Klik untuk mengurutkan data" style="width: 0px;">Jarak Tempuh (km)</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 0px;">Status</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Aksi: Klik untuk mengurutkan data" style="width: 0px;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>  
                                @foreach($vehicles as $v)
                                @if($loop->iteration % 2 == 0)									
                                <tr class="odd">
                                    <td class="" tabindex="0">{{ $v->plat_number}}</td>
                                    <td style="">{{ $v->brand }}</td>
                                    <td style="">{{ $v->model_version }}</td>
                                    <td style="">{{ $v->km }}</td>
                                    @if($v->status->id == 1)
                                    <td style=""><span class="badge bg-success text-white">{{ $v->status->status }}</span></td>
                                    @elseif($v->status->id == 2 )
                                    <td style=""><span class="badge bg-danger text-white">{{ $v->status->status }}</span></td>
                                    @else
                                    <td style=""><span class="badge">{{ $v->status->status }}</span></td>
                                    @endif
                                    <td class="table-action">
                                @if(isset($v->deleted_at))           
                                    <a href="#" class="btn btn-secondary btn-sm disabled">Tidak Aktif</a>
                                @else
                                    <a class="edit-icon" href="#" data-toggle="modal" data-target="#vehicle-form-modal" 
                                        data-car='{ "id": "{{ $v->id }}", 
                                                    "plat": "{{ $v->plat_number }}",
                                                    "brand": "{{ $v->brand }}",
                                                    "version": "{{ $v->model_version}}",
                                                    "status": "{{ $v->status->id }}",
                                                    "km": "{{ $v->km }}"
                                                    }'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 align-middle">
                                        <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                        </svg>
                                    </a>
                                    <a class="delete-icon" href="#" data-toggle="modal" data-target="#delete-row" data-id="{{ $v->id }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash align-middle">
                                        <polyline points="3 6 5 6 21 6"></polyline>
                                        <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                        </path>
                                        </svg>
                                    </a>
                                @endif
                                </td>
                            </tr>
                            @else
                            <tr class="even">
                                <td class="" tabindex="0">{{ $v->plat_number}}</td>
                                <td style="">{{ $v->brand }}</td>
                                <td style="">{{ $v->model_version }}</td>
                                <td style="">{{ $v->km }}</td>
                                @if($v->status->id == 1)
                                <td style=""><span class="badge bg-success text-white">{{ $v->status->status }}</span></td>
                                @elseif($v->status->id == 2 )
                                <td style=""><span class="badge bg-danger text-white">{{ $v->status->status }}</span></td>
                                @else
                                <td style=""><span class="badge">{{ $v->status->status }}</span></td>
                                @endif
                                <td class="table-action">
                                @if(isset($v->deleted_at))
                                <a href="#" class="btn btn-secondary btn-sm disabled">Tidak Aktif</a>
                                @else
                                <a class="edit-icon" href="#" data-toggle="modal" data-target="#vehicle-form-modal" 
                                    data-car='{ "id": "{{ $v->id }}", 
                                                "plat": "{{ $v->plat_number }}",
                                                "brand": "{{ $v->brand }}",
                                                "version": "{{ $v->model_version}}",
                                                "status": "{{ $v->status->id }}",
                                                "km": "{{ $v->km }}"
                                                }'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 align-middle">
                                    <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                    </svg>
                                </a>
                                <a class="delete-icon" href="#" data-toggle="modal" data-target="#delete-row" data-id="{{ $v->id }}">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash align-middle">
                                    <polyline points="3 6 5 6 21 6"></polyline>
                                    <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                    </path>
                                    </svg>
                                </a>
                                @endif
                                </td>
                            </tr>
                            @endif
                            @endforeach
                            </tbody>
                        </table>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-6">
                                <div class="dataTables_paginate paging_simple_numbers" id="zero_config_paginate">
                                    <ul class="pagination mt-3">
                                    {{ $vehicles->links() }}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-script')

<script>

$(document).ready(function() {

var table = $('#zero_config').DataTable( {
    paging: false,
    info: false
  });

  $(".delete-icon").click(function() {
    var id = $(this).data('id');
    $(".m-delete-btn").click(function() {
      var url = "{{ route('kendaraan.delete', ':id') }}";
      url = url.replace(':id', id);
      window.location.href = url;
    });
  });


  $(".btn-add-new").click(function() {

    document.getElementById('vehicle-form-label').textContent = 'Tambah Kendaraan';

    $('#vehicle-form-modal').on('shown.bs.modal', function() {
        var modal = $(this);
        modal.find('#id').val("");
        modal.find('#plat_number').val("");
        modal.find('#brand').val("");
        modal.find('#model_version').val("");
        modal.find('#km').val("");
        modal.find('#vehicle-form').attr("action", "{{ route('kendaraan.save') }}");
    });
  });


  $(".edit-icon").click(function() {
    var car = $(this).data('car');

    document.getElementById('vehicle-form-label').textContent = 'Ubah Kendaraan';

     $('#vehicle-form-modal').on('shown.bs.modal', function() {
        var modal = $(this);
        modal.find('#id').val(car.id);
        modal.find('#plat_number').val(car.plat);
        modal.find('#brand').val(car.brand);
        modal.find('#model_version').val(car.version);
        modal.find('#km').val(car.km);

        modal.find('#vehicle-form').attr("action", "{{ route('kendaraan.update') }}");
     });
  });
});

</script>

@endsection
