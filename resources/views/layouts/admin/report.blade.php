
@extends($user_role == 1 ? 'layouts/base/navadmin' : 'layouts/base/navrequestor')

@section('page-title')
Laporan Peminjaman
@endsection

@section('subtitle')
Anda bisa melihat data peminjaman yang sudah selesai disini                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
@endsection

@section('custom-styles')
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <p style="margin-left:12px"><strong>Tampilkan data pada bulan dan tahun: </strong></p>
                </div>
                <form id="form-filter" method="POST" action="laporan">
                    @csrf
                <div class="row mb-3">
                    <div class="col-md-2 col-sm-3">
                        <select class="form-control form-select" aria-label="Pilih Bulan" id="month" name="month">
                            @foreach($months as $num => $name)
                                @if($num == $currentMonth) 
                                <option value="{{ $num }}" selected>{{ $name }}</option>
                                @else
                                <option value="{{ $num }}">{{ $name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2 col-sm-3">
                        <select class="form-control form-select" aria-label="Pilih Tahun" id="year" name="year">
                            @foreach($years as $y)
                                @if($y == $currentYear)
                                <option value="{{ $y }}" selected>{{ $y }}</option>
                                @else
                                <option value="{{ $y }}">{{ $y }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2 col-sm-3">
                        <button type="submit" class="btn btn-primary">Tampilkan</button>
                    </div>
                   
                    <div class="col-md-6"></div>
                    <div class="col-md-3 col-sm-3 mt-3 text-end">
                        <button id="btn_export" type="button" class="btn btn-sm btn-outline-success btn-rounded"><i class="fas fa-file-excel"></i> Ekspor Data</button>
                    </div>
                </div>
                
                </form>
                <div class="table-responsive">
                    <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                       
                        <div class="row">
                        <div class="col-sm-12">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap dataTable" role="grid" aria-describedby="zero_config_info">
                            <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Nama Pegawai: Klik untuk mengurutkan data" style="width: 0px;">Nama Pegawai</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Keperluan: Klik untuk mengurutkan data" style="width: 0px;">Keperluan</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Tujuan: Klik untuk mengurutkan data" style="width: 0px;">Tujuan</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Pengemudi: Klik untuk mengurutkan data" style="width: 0px;">Pengemudi</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Tanggal Berangkat: Klik untuk mengurutkan data" style="width: 0px;">Tanggal Berangkat</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Tanggal Pulang: Klik untuk mengurutkan data" style="width: 0px;">Tanggal Pulang</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Tanggal Pulang: Klik untuk mengurutkan data" style="width: 0px;">Total Biaya</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Aksi: Klik untuk mengurutkan data" style="width: 0px;">Detail</th>
                                </tr>
                            </thead>
                            <tbody>	
                            @foreach($bookings as $b)
                                @if($loop->iteration % 2 == 0)									
                                <tr class="odd">
                                    <td tabindex="0">{{ $b->requestor }}</td>
                                    <td>{{$b->request->requisite}}</td>
                                    <td>{{$b->request->destination}}</td>
                                    <td>{{$b->driver}}</td>
                                    <td style="">{{$b->exit_date}}</td>
                                    <td style="">{{$b->return_date }}</td>
                                    <td style="">{{$b->total_cost}}</td>
                                    <td class="table-action">
                                        <a class="detail-icon" href="/laporan/{{$b->id}}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text align-middle me-2 text-primary"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                                        </a>
                                    </td>
                                </tr>

                                @else
                                <tr class="even">
                                    <td tabindex="0">{{ $b->requestor }}</td>
                                    <td>{{$b->request->requisite}}</td>
                                    <td>{{$b->request->destination}}</td>
                                    <td>{{$b->driver}}</td>
                                    <td style="">{{$b->exit_date}}</td>
                                    <td style="">{{$b->return_date }}</td>
                                    <td style="">{{$b->total_cost}}</td>
                                    <td class="table-action">
                                        <a class="detail-icon" href="/laporan/{{$b->id}}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text align-middle me-2 text-primary"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                                        </a>
                                    </td>
                                </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-6">
                                <div class="dataTables_paginate paging_simple_numbers" id="zero_config_paginate">
                                    <ul class="pagination mt-3">
                                    {{ $bookings->links() }}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-script')
<script>

$(document).ready(function() {
    $('#btn_export').on('click', function() {
        $('#form-filter').attr("action", "{{ route('laporan.export') }}");
        document.getElementById('form-filter').submit();
    });
});

</script>
@endsection