@extends('layouts/base/navadmin')

@section('page-title')
@if($data->status_id == 1 && $action == 'validasi')
    Validasi
@elseif($data->status_id == 1 && $action == 'tolak')
    Tolak
@elseif($data->status_id == 2 && $action == 'ubah')
    Ubah
@else 
    Detail
@endif Pengajuan
@endsection

@section('subtitle')
Detail pengajuan peminjaman kendaraan
@endsection


@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
           
            @if($action == 'validasi' || $action == 'ubah')
            <form method="post" action="/pengajuan/save">
            @elseif($action == 'tolak' || $action == 'batalkan')
            <form method="post" action="/pengajuan/decline">
            @else
            <form method="post">
            @endif                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
            
            <input name="action" value="{{$action}}" hidden>
            <table class="table table-md mt-2 mb-5">
                <tbody>
                    <tr class="mb-2">
                        <th>Nomor Pengajuan</th>
                        <td>: {{$data->request_number}}</td>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr class="mb-2">
                        <th>Tanggal Pengajuan</th>
                        <td>: {{$data->created_at->locale('id')->isoFormat('dddd, D MMMM Y') }}</td>
                        <th>Tujuan</th>
                        <td>: {{ $data->destination }}</td>
                    </tr>
                    <tr>
                        <th>Nama Pegawai</th>
                        <td>: {{ $data->employee->name }}</td>
                        <th>Tanggal Berangkat</th>
                        <td>: {{ $data->travel_date }}</td>
                    </tr>
                    <tr>
                        <th>Keperluan</th>
                        <td>: {{ $data->requisite->requisite }}</td>
                        <th>Jam Berangkat</th>
                        <td>: {{ $data->travel_time }}</td>
                    </tr>
                    <tr>
                        <th>Jumlah Penumpang</th>
                        <td>: {{$data->total_passenger}} Orang</td>
                        <th>Status</th>
                        @if($data->status_id == 1)
                        <td>: <span class="badge bg-success text-white">{{ $data->status->status }}</span></td>
                        @elseif($data->status_id == 2)
                        <td>: <span class="badge bg-warning text-white">{{ $data->status->status }}</span></td>
                        @elseif($data->status_id == 3)
                        <td>: <span class="badge bg-danger text-white">{{ $data->status->status }}</span></td>
                        @else
                        <td>: <span class="badge bg-secondary text-white">{{ $data->status->status }}</span></td>
                        @endif
                    </tr>
                    @csrf
                    <input type="hidden" id="id" name="id" value="{{$data->id}}">
                    <tr>
                        @if($data->status_id == 1 && $action=='validasi' || $data->status_id == 2 && $action=='ubah')
                        <th>Pilih Driver</th>
                        <td><select style="width:180px;" class="form-control form-select" aria-label="Pilih Driver" id="driver" name="driver">
                                <option value="0" selected>Pilih Driver</option>
                                @if($drivers != NULL && count($drivers) > 0)
                                    @foreach($drivers as $d)
                                        <option value="{{$d->id}}">{{$d->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <a type="button" class="btn btn-sm btn-success mt-2" href="/master/pengemudi">cek pengemudi <i class="fas fa-share-square"></i></a>
                        </td>
                        @elseif($data->status_id == 1 && $action=='tolak' || $data->status_id == 2 && $action=='batalkan')
                        <th>Keterangan</th>
                        <td>
                            <textarea class="form-control"  id="note" name="note"></textarea>
                        </td>
                        @elseif($data->status_id == 3)
                        <th>Keterangan Penolakan</th>
                        <td class="text-danger">: {{ $data->notes }}</td>
                        @else
                        <th>Driver</th>
                        <td>: {{$data->driver_name}}</td>
                        @endif

                        @if($data->status_id > 1 && $data->status_id != 3)
                        <th>Nomor Peminjaman</th>
                        <td>: <a href="/peminjaman/{{$data->booking_id}}">{{$data->booking_number}}</a></td>
                        @endif
                    </tr>
                </tbody>
             </table>
                @if($data->status_id == 1 || ($data->status_id == 2 && $data->driver_id == null))
                <div class="row justify-content-md-center">
                    @if($action == 'validasi' || $action == 'ubah')
                    <button type="submit" class="col-md-2 btn btn-primary btn-save" style="margin-right:10px">Simpan </button>
                    @elseif($action == 'tolak')
                    <button type="submit" class="col-md-2 me-2 btn btn-danger btn-decline">Tolak </button>
                    @elseif($action == 'batalkan')
                    <button type="submit" class="col-md-2 btn btn-danger btn-decline">Batalkan </button>
                    @endif
                    <a href="/pengajuan" class="col-md-2 ms-3 btn btn-secondary">Batal </a>
                </div>
                @endif
            </form>
            </div>
        </div>
    </div>
</div>
@endsection
