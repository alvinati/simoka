@extends('layouts/base/navadmin')

@section('page-title')
Data Peminjaman Kendaraan
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            
                <div id="cancel-book" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none; padding-right: 17px;" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Batalkan Peminjaman</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <form method="POST" action="/peminjaman/cancel">
                            @csrf
                                <input type="hidden" id="id" name="id" value="">
                                <div class="modal-body">
                                    <h5>Apa Anda yakin untuk membatalkan peminjaman?</h5>
                                    <h6>Mohon tuliskan alasan pembatalan</h6>
                                    <textarea class="form-control" name="notes"></textarea>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light" data-dismiss="modal">Tidak</button>
                                    <button type="submit" class="btn btn-danger m-delete-btn">Ya</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>

                 <!-- /.tabel-content -->
                <div class="table-responsive">
                    <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="zero_config" class="table table-striped table-bordered no-wrap dataTable" role="grid" aria-describedby="zero_config_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Nomor: Klik untuk mengurutkan data" style="width: 0px;">Nomor</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="nama Pegawai: Klik untuk mengurutkan data" style="width: 0px;">Nama Pegawai</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Tanggal Berangkat: Klik untuk mengurutkan data" style="width: 0px;">Tanggal Berangkat</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Keperluan: Klik untuk mengurutkan data" style="width: 0px;">Keperluan</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Tujuan: activate to sort column ascending" style="width: 0px;">Tujuan</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Tanggal Selsai: Klik untuk mengurutkan data" style="width: 0px;">Tanggal Selesai</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Status: Klik untuk mengurutkan data" style="width: 0px;">Status</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Aksi: Klik untuk mengurutkan data" style="width: 0px;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>	
                                    @foreach($bookings as $b)
                                        @if($loop->iteration % 2 == 0)									
                                        <tr class="odd">
                                            <td tabindex="0">{{ $b->book_number }}</td>
                                            <td>{{$b->requestor}}</td>
                                            <td style="">{{$b->exit_date}}</td>
                                            <td >{{$b->request->requisite}}</td>
                                            <td style="">{{ $b->request->destination }}</td>
                                            
                                            @if($b->return_date != null ) 
                                            <td style="">{{$b->return_date }}</td>
                                            @else 
                                            <td style="-"></td>
                                            @endif

                                            @if($b->status_id == 1) 
                                            <td><span class="badge bg-success text-white">{{$b->status->status}}</span></td>
                                            @else
                                            <td><span class="badge bg-secondary text-white">{{$b->status->status}}</span></td>
                                            @endif
                                            
                                            <td class="table-action">
                                            @if($b->status_id == 1)
                                                <a  data-toggle="tooltip" data-placement="top" title="" data-original-title="Selesaikan Peminjaman" class="accept-icon" href="/peminjaman/{{ $b->id }}">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle align-middle text-success"><path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path><polyline points="22 4 12 14.01 9 11.01"></polyline></svg>
                                                </a>
                                            @else 
                                                <a  data-toggle="tooltip" data-placement="top" title="" data-original-title="Lihat Detail" class="detail-icon" href="/peminjaman/{{ $b->id }}">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text align-middle me-2 text-primary"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                                                </a>    
                                            @endif

                                            @if($b->isCancelable)
                                                <a title="Batalkan Peminjaman" data-toggle="modal" data-target="#cancel-book" data-id="{{$b->id}}" class="cancel-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle align-middle text-danger"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                                                </a>
                                            @endif
                                            </td>
                                        </tr>

                                        @else
                                        <tr class="even">
                                        <td tabindex="0">{{ $b->book_number }}</td>
                                            <td>{{$b->requestor}}</td>
                                            <td style="">{{$b->exit_date}}</td>
                                            <td >{{$b->request->requisite}}</td>
                                            <td style="">{{ $b->request->destination }}</td>
                                        
                                            @if($b->return_date != null ) 
                                            <td style="">{{$b->return_date }}</td>
                                            @else 
                                            <td style="-"></td>
                                            @endif

                                            @if($b->status_id == 1) 
                                            <td><span class="badge bg-success text-white">{{$b->status->status}}</span></td>
                                            @else
                                            <td><span class="badge bg-secondary text-white">{{$b->status->status}}</span></td>
                                            @endif

                                            <td class="table-action">
                                            @if($b->status_id == 1)
                                                <a  data-toggle="tooltip" data-placement="top" title="" data-original-title="Selesaikan Peminjaman" class="accept-icon" href="/peminjaman/{{ $b->id }}">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle align-middle text-success"><path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path><polyline points="22 4 12 14.01 9 11.01"></polyline></svg>
                                                </a>
                                            @else 
                                                <a  data-toggle="tooltip" data-placement="top" title="" data-original-title="Lihat Detail" class="detail-icon" href="/peminjaman/{{ $b->id }}">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text align-middle me-2 text-primary"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                                                </a>    
                                            @endif

                                            @if($b->isCancelable)
                                                <a title="Batalkan Peminjaman" data-toggle="modal" data-target="#cancel-book" data-id="{{$b->id}}" class="cancel-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle align-middle text-danger"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                                                </a>
                                            @endif
                                            </td>
                                        </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-6">
                                <div class="dataTables_paginate paging_simple_numbers" id="zero_config_paginate">
                                    <ul class="pagination mt-3">
                                    {{ $bookings->links() }}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-script')
<script>
$(document).ready( function () {
  var table = $('#zero_config').DataTable( {
    paging: false,
    info: false
  });

  $('.cancel-icon').on('click', function() {
    var id = $(this).data('id');

    $('#cancel-book').on('shown.bs.modal', function() { 
        $(this).find('#id').val(id);
    });
  });

});
  
</script>

@endsection