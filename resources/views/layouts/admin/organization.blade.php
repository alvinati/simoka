@extends('layouts/base/navadmin')

@section('page-title')
Master data Jabatan & Divisi
@endsection

@section('subtitle')
Anda dapat mengubah Jabatan & Divisi disini
@endsection

@section('content')
<div class="row">
 <!--  Modal division edit form -->
<div class="modal fade" id="edit-division-modal" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <form method="post" id="form-division" action="/master/divisi-jabatan/simpan">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Ubah Divisi</h5>
                    <button id="btn-close-division-modal" type="button" class="close" data-dismiss="modal" aria-label="Close">x</button>
                </div>
                <div class="modal-body m-3">
                    <div class="row">
                        <div class="col-md-1">No</div>
                        <div class="col-md-8">Nama</div>
                    </div>
                    @csrf
                    <input name="change_key" value="division" class="form-control" hidden/>
                    @foreach($division as $key=>$d) 
                    <div class="row mt-2">
                        <div class="col-md-1">{{$key+1}}<input class="form-control" name="ids[]" value="{{$d->id}}" hidden/></div>
                        <div class="col-md-8"><input class="form-control" name="divisions[]" value="{{$d->name}}"/></div>
                    </div>
                    @endforeach
                    <div id="new-row"></div>
                    <div class="row justify-content-md-center">
                        <button type="button" id="add-division" class="btn btn-outline-primary col-11 mt-4">Tambah Divisi</button>    
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="save-btn">Simpan</button>
                </div>
            </div>
        </form><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--  Modal title edit form -->
<div class="modal fade" id="edit-title-modal" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <form method="post" id="form-division" action="/master/divisi-jabatan/simpan">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Ubah Jabatan</h5>
                    <button id="btn-close-title-modal" type="button" class="close" data-dismiss="modal" aria-label="Close">x</button>
                </div>
                <div class="modal-body m-3">
                    <div class="row">
                        <div class="col-md-1">No</div>
                        <div class="col-md-8">Nama</div>
                    </div>
                    @csrf
                    <input name="change_key" value="title" class="form-control" hidden/>
                    @foreach($title as $key=>$t) 
                    <div class="row mt-2">
                        <div class="col-md-1">{{$key+1}}<input class="form-control" name="ids[]" value="{{$t->id}}" hidden/></div>
                        <div class="col-md-8"><input class="form-control" name="titles[]" value="{{$t->name}}"{{ $t->id == 3 ? 'disabled' : '' }}/></div>
                    </div>
                    @endforeach
                    <div id="new-row-title"></div>
                    <div class="row justify-content-md-center">
                        <button type="button" id="add-title" class="btn btn-outline-primary col-11 mt-4">Tambah Jabatan</button>    
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="save-btn">Simpan</button>
                </div>
            </div>
        </form><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--  Modal delete data -->
<div id="delete-row" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none; padding-right: 17px;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Hapus Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h6>Apa Anda yakin?</h6>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger m-delete-btn">Hapus</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="col-lg-6">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-12">
                    <h4 class="card-title">Divisi</h4>
                    <h6 class="card-subtitle">Tambahkan / Ubah / Hapus master data Divisi</h6>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-12 d-flex flex-row-reverse">
                    <button id="btn-change-division"  type="button" style="height:30px;" class="btn btn-sm btn-outline-primary btn-rounded btn-edit" 
                        data-toggle="modal" data-target="#edit-division-modal" data-last="{{count($division)}}">
                            <i class="fas fa-edit"></i> 
                            Ubah Divisi
                    </button>
                </div>
            </div>
            
            <div class="table-responsive">
                <table class="table">
                    <thead class="bg-primary text-white">
                        <tr>
                            <th>No</th>
                            <th>Nama Divisi</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($division as $key=>$d)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td class="d-none d-xl-table-cell">{{$d->name}}</td>
                            <td class="d-none d-xl-table-cell">
                                <a class="delete-icon" href="#" data-toggle="modal" data-target="#delete-row" data-origin="division" data-id="{{ $d->id }}">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash align-middle">
                                    <polyline points="3 6 5 6 21 6"></polyline>
                                    <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                    </path>
                                    </svg>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-6">
    <div class="card">
    <div class="card-body">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-12">
                    <h4 class="card-title">Jabatan</h4>
                    <h6 class="card-subtitle">Tambahkan / Ubah / Hapus master data Jabatan</h6>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-12 d-flex flex-row-reverse">
                    <button  type="button" style="height:30px;" class="btn btn-sm btn-outline-primary btn-rounded btn-edit" data-toggle="modal" data-target="#edit-title-modal"><i class="fas fa-edit"></i> Ubah Jabatan</button>
                </div>
            </div>
            
            <div class="table-responsive">
                <table class="table">
                    <thead class="bg-primary text-white">
                        <tr>
                            <th>No</th>
                            <th>Nama Jabatan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($title as $key=>$t)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td class="d-none d-xl-table-cell">{{$t->name}}</td>
                            <td class="d-none d-xl-table-cell">
                                <a class="delete-icon" href="#" data-toggle="modal" data-target="#delete-row" data-origin="title" data-id="{{ $t->id }}">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash align-middle">
                                    <polyline points="3 6 5 6 21 6"></polyline>
                                    <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                    </path>
                                    </svg>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('custom-script')
<script>
$(document).ready(function() {

var lastDivNo = $('#btn-change-division').data('last');   
var lastTittleNo = $('#btn-change-title').data('last');

$(".delete-icon").click(function() {
    var id = $(this).data('id');
    var origin = $(this).data('origin');
    $(".m-delete-btn").click(function() {
      var url = origin == "division" ? "{{ route('division.delete', ':id') }}" : "{{ route('title.delete', ':id') }}";
      url = url.replace(':id', id);
      window.location.href = url;
    });
});

$('#edit-division-modal').on('shown.bs.modal', function() {
    var modal = $(this);
    modal.find('#save-btn').on('click',function() {
        document.getElementById('division-form').submit();
    });

    var btnAdd = $('#add-division');
    btnAdd.prop("disabled", lastDivNo >=20);
});

$("#btn-close-division-modal").on('click', function() {
    var existingDataCount = $('#btn-change-division').data('last');
    
    console.log("countData: "+existingDataCount);
    var addedRow = lastDivNo - existingDataCount;
    console.log("addedRow: "+addedRow);
    if(addedRow > 0) {
        for(var i = 1; i == addedRow; i++) {
            var addedRow = existingDataCount+i;
            console.log("i-divisions-"+addedRow);
            var inputEl = document.getElementById("i-divisions-"+existingDataCount+i);
            console.log(inputEl);
        }
    }
});

$('#titleFromModal').on('shown.bs.modal', function() {
    var modal = $(this);
    modal.find('#save-btn').on('click', function() {
        document.getElementById('titleForm').submit();
    });

    var btnAdd = $('#add-title');
    btnAdd.prop("disabled", lastTittleNo >= 20);
});

$('#add-division').on('click', function() {
    lastDivNo = lastDivNo +1;
    var newNode = createNewInputNode(lastDivNo, "divisions");
    document.getElementById('new-row').appendChild(newNode);
  
    var btnAdd = $('#add-division');
    btnAdd.prop("disabled", lastDivNo >=20);
});

$('#add-title').on('click', function() {
    lastTittleNo = lastTittleNo +1;
    var newNode = createNewInputNode(lastTittleNo, "titles");
    document.getElementById('new-row-title').appendChild(newNode);
   
    var btnAdd = $('#add-title');
    btnAdd.prop("disabled", lastTittleNo >= 20);
});
});

function createNewInputNode(newNum, key) {
    var rowNode = document.createElement('DIV');
    rowNode.setAttribute("class","row mt-3");
    var id = "i-"+key+"-"+newNum;
    rowNode.setAttribute("id", id);
    console.log(id);

    var numNode = document.createElement('DIV');
    numNode.setAttribute("class", "col-md-1");

    var inputNode = document.createElement('DIV');
    inputNode.setAttribute("class", "col-md-8");
    var input = document.createElement('INPUT');
    input.setAttribute("class", "form-control");
    input.setAttribute("name", key+"[]");
    inputNode.appendChild(input);

    var delNode = document.createElement('A');
    delNode.setAttribute("id", "del-input");
    delNode.setAttribute("class", "col-md-1");
    delNode.setAttribute("style", "color:red;");
    delNode.setAttribute("onclick", "deleteInput('"+id+"')");
    var icon = document.createElement('i');
    icon.setAttribute("class", "far fa-times-circle mt-2 p-0");
    delNode.appendChild(icon);
    console.log(delNode);

    rowNode.appendChild(numNode);
    rowNode.appendChild(inputNode);
    rowNode.appendChild(delNode);

    return rowNode;
}

function deleteInput(id) {
    var inputEl = document.getElementById(id);
    inputEl.remove();
}

</script>
@endsection