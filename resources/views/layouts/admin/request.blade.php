@extends('layouts/base/navadmin')

@section('page-title')
Data Pengajuan
@endsection

@section('subtitle')
Anda dapat melakukan validasi / menolak pengajuan pinjaman kendaraan disini
@endsection


@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- /.tabel-content -->
                <div class="table-responsive">
                    <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                       
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="zero_config" class="table table-striped table-bordered no-wrap dataTable" role="grid" aria-describedby="zero_config_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Nomor: Klik untuk mengurutkan data" style="width: 0px;">Nomor</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Tanggal Pengajuan: Klik untuk mengurutkan data" style="width: 0px;">Tanggal Pengajuan</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Nama Pegawai: Klik untuk mengurutkan data" style="width: 0px;">Nama Pegawai</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Tujuan: Klik untuk mengurutkan data" style="width: 0px;">Tujuan</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Tanggal Berangkat: activate to sort column ascending" style="width: 0px;">Tanggal Berangkat</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Jam Berangkat: Klik untuk mengurutkan data" style="width: 0px;">Jam Berangkat</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Status: Klik untuk mengurutkan data" style="width: 0px;">Status</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Aksi: Klik untuk mengurutkan data" style="width: 0px;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>  
                                    @foreach($requests as $r)
                                        @if($loop->iteration % 2 == 0)									
                                        <tr class="odd">
                                            <td tabindex="0">{{ $r->request_number }}</td>
                                            <td style="">{{ $r->created_at->locale('id')->isoFormat('dddd, D MMMM Y')}}</td>
                                            <td style="">{{ $r->employee->name }}</td>
                                            <td style="">{{ $r->destination }}</td>
                                            <td style="">{{ $r->travel_date }}</td>
                                            <td style="">{{ $r->travel_time }}</td>

                                            @if($r->status_id == 1)
                                            <td><span class="badge bg-success text-white">{{ $r->status->status }}</span></td>
                                            @elseif($r->status_id == 2)
                                            <td><span class="badge bg-warning text-white">{{ $r->status->status }}</span></td>
                                            @elseif($r->status_id == 3)
                                            <td><span class="badge bg-danger text-white">{{ $r->status->status }}</span></td>
                                            @else
                                            <td><span class="badge bg-secondary text-white">{{ $r->status->status }}</span></td>
                                            @endif

                                            <td class="table-action">
                                                @if($r->status_id == 1)
                                                <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Validasi Pengajuan" class="accept-icon" href="/pengajuan/{{ $r->id }}/validasi">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle align-middle text-success"><path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path><polyline points="22 4 12 14.01 9 11.01"></polyline></svg>
                                                </a>
                                                <a data-toggle="tooltip" data-placement="right" title="" data-original-title="Tolak Pengajuan"  class="decline-icon" href="/pengajuan/{{ $r->id }}/tolak">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle align-middle text-danger"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                                                </a>
                                                @elseif($r->status_id == 2 && $r->driver_id == NULL)
                                                <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Ubah Pengajuan" class="edit-icon" href="/pengajuan/{{ $r->id }}/ubah">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 align-middle"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                                        </svg>
                                                </a>
                                                <a data-toggle="tooltip" data-placement="right" title="" data-original-title="Batalkan Pengajuan"  class="cancel-icon" href="/pengajuan/{{ $r->id }}/batalkan">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle align-middle text-danger"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                                                </a>
                                                @else
                                                <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Lihat Detail"  class="detail-icon" href="/pengajuan/{{ $r->id }}/detail">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text align-middle me-2 text-primary"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                                                </a>
                                                @endif
                                            </td>
                                        </tr>
                                            @else
                                        <tr class="even">
                                            <td tabindex="0">{{ $r->request_number }}</td>
                                            <td style="">{{ $r->created_at->locale('id')->isoFormat('dddd, D MMMM Y')}}</td>
                                            <td style="">{{ $r->employee->name }}</td>
                                            <td style="">{{ $r->destination }}</td>
                                            <td style="">{{ $r->travel_date }}</td>
                                            <td style="">{{ $r->travel_time }}</td>
                                        
                                            @if($r->status_id == 1)
                                            <td><span class="badge bg-success text-white">{{ $r->status->status }}</span></td>
                                            @elseif($r->status_id == 2)
                                            <td><span class="badge bg-warning text-white">{{ $r->status->status }}</span></td>
                                            @elseif($r->status_id == 3)
                                            <td><span class="badge bg-danger text-white">{{ $r->status->status }}</span></td>
                                            @else
                                            <td><span class="badge bg-secondary text-white">{{ $r->status->status }}</span></td>
                                            @endif

                                            <td class="table-action">
                                                @if($r->status_id == 1)
                                                <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Validasi Pengajuan" class="accept-icon" href="/pengajuan/{{ $r->id }}/validasi">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle align-middle text-success"><path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path><polyline points="22 4 12 14.01 9 11.01"></polyline></svg>
                                                </a>
                                                <a data-toggle="tooltip" data-placement="right" title="" data-original-title="Tolak Pengajuan"  class="decline-icon" href="/pengajuan/{{ $r->id }}/tolak">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle align-middle text-danger"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                                                </a>
                                                @elseif($r->status_id == 2 && $r->driver_id == NULL)
                                                <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Ubah Pengajuan" class="edit-icon" href="/pengajuan/{{ $r->id }}/ubah">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 align-middle"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                                        </svg>
                                                </a>
                                                <a data-toggle="tooltip" data-placement="right" title="" data-original-title="Batalkan Pengajuan"  class="cancel-icon" href="/pengajuan/{{ $r->id }}/batalkan">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle align-middle text-danger"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                                                </a>
                                                @else
                                                <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Lihat Detail"  class="detail-icon" href="/pengajuan/{{ $r->id }}/detail">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text align-middle me-2 text-primary"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                                                </a>
                                                @endif
                                            </td>
                                        </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-6">
                                <div class="dataTables_paginate paging_simple_numbers" id="zero_config_paginate">
                                    <ul class="pagination mt-3">
                                    {{ $requests->links() }}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-script')
<script>
$(document).ready( function () {
  var table = $('#zero_config').DataTable( {
    paging: false,
    info: false,
    order:[]
  });
});
  
</script>

@endsection