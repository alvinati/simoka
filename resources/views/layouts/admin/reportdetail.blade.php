@extends($user_role == 1 ? 'layouts/base/navadmin' : 'layouts/base/navrequestor')

@section('page-title')
Detail Laporan
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                <thead>
                  <tr>
                    <th>Nama Pegawai</th>
                    <th>{{$data->employee->name}}</th>
                  </tr>
                </thead>
                <tbody>	
                    <tr>
                        <th>Divisi</th>
                        <td>{{$data->employee->division->name}}</td>
                    </tr>
                    <tr>
                        <th>Jabatan</th>
                        <td>{{$data->employee->title->name}}</td>
                    </tr>
                    <tr>
                        <th>Driver</th>
                        <td>{{$data->driver}}</td>
                    </tr>
                    <tr>
                        <th>Mobil</th>
                        <td>{{$data->car}}</td>
                    </tr>
                    <tr>
                        <th>Keperluan</th>
                        <td>{{$data->request->requisite}}</td>
                    </tr>
                    <tr>
                        <th>Tujuan</th>
                        <td>{{$data->request->destination}}</td>
                    </tr>
                    <tr>
                        <th>Tanggal Berangkat</th>
                        <td>{{$data->exit_date}}</td>
                    </tr>
                    <tr>
                        <th>Tanggal Pulang</th>
                        <td>{{$data->return_date}}</td>
                    </tr>
                    <tr>
                        <th>Biaya Bensin</th>
                        <td>{{$data->fuel_cost}}</td>
                    </tr>
                    <tr>
                        <th>Biaya Tol</th>
                        <td>{{$data->toll_cost}}</td>
                    </tr>
                    <tr>
                        <th>Total Biaya</th>
                        <td>{{$data->total_cost}}</td>
                    </tr>
                </tbody>
                </table>
                </div>
            </div>
            
        </div>

        <div class="row justify-content-end" >
                <a class="btn btn-primary me-3" href="/laporan">Kembali</a>
            </div>
    </div>
</div>
@endsection