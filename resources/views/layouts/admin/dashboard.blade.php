@extends('layouts/base/navadmin')

@section('page-title')
Selamat Datang, {{$user_fullname}}
@endsection

@section('subtitle')
<h5 style="color:#8392a5">{{$currentDate}}</h5>
@endsection

@section('content')
<div class="row">
<div class="col-lg-6">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Pengajuan Hari ini</h4>
            @if(count($requests) < 1)
            <div class="text-center mt-5 mb-5">
                <h4><strong>Belum ada Pengajuan</strong></h4>
            </div>
            @else
            <h6 class="card-subtitle">Informasi diurutkan berdasar Tanggal Berangkat</h6>
            <div class="table-responsive">
                <table class="table">
                    <thead class="bg-primary text-white">
                        <tr>
                            <th>#</th>
                            <th>Nama Pegawai</th>
                            <th>Tanggal Berangkat</th>
                            <th>Jam Berangkat</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($requests as $key=>$r)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$r->employee->name}}</td>
                            <td>{{$r->travel_date}}</td>
                            <td>{{$r->travel_time}}</td>
                            <td class="table-action">
                            @if($r->status_id == 1)
                            <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Validasi Pengajuan" class="accept-icon" href="/pengajuan/{{ $r->id }}/validasi">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle align-middle text-success"><path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path><polyline points="22 4 12 14.01 9 11.01"></polyline></svg>
                            </a>
                            <a data-toggle="tooltip" data-placement="right" title="" data-original-title="Tolak Pengajuan"  class="decline-icon" href="/pengajuan/{{ $r->id }}/tolak">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle align-middle text-danger"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                            </a>
                            @elseif($r->status_id == 2 && $r->driver_id == NULL)
                            <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Ubah Pengajuan" class="edit-icon" href="/pengajuan/{{ $r->id }}/ubah">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 align-middle"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                    </svg>
                            </a>
                            <a data-toggle="tooltip" data-placement="right" title="" data-original-title="Batalkan Pengajuan"  class="cancel-icon" href="/pengajuan/{{ $r->id }}/batalkan">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle align-middle text-danger"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                            </a>
                            @else
                            <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Lihat Detail"  class="detail-icon" href="/pengajuan/{{ $r->id }}/detail">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text align-middle me-2 text-primary"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                            </a>
                            @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif
        </div>
    </div>
</div>
<div class="col-lg-6">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Status Pengemudi</h4>
            <h6 class="card-subtitle">Informasi status pengemudi hari ini</h6>
            <div class="table-responsive">
                <table class="table">
                    <thead class="bg-success text-white">
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Kendaraan</th>
                            <th>Plat Nomor</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($drivers as $key=>$d)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$d->employee->name}}</td>
                            
                            <td>@if($d->car != null){{$d->car->model_version}}@else - @endif</td>
                            <td>@if($d->car != null){{$d->car->plat_number}}@else - @endif</td>
                            
                            <td style="">
                            @if($d->status_id == 1 || $d->status_id == 6)
                            <span class="badge bg-success text-white">{{ $d->status->status }}</span> 
                            @elseif($d->status_id == 2)
                            <span class="badge bg-warning text-white">{{ $d->status->status }}</span>
                            @else 
                            <span class="badge bg-danger text-white">{{ $d->status->status }}</span> 
                            @endif 
                            
                            @if($d->availability != null )
                                @if($d->availability_id == 1)
                                <span class="badge bg-success text-white">{{ $d->availability->status }}</span> 
                                @else
                                <span class="badge bg-warning text-white">{{ $d->availability->status }}</span>
                                @endif
                            @endif
                                
                            </td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
@endsection