@extends('layouts/base/navadmin')

@section('page-title')
Data Pegawai
@endsection

@section('subtitle')
Anda dapat mengatur data pegawai disini
@endsection

@section('right-action-button')
<button  type="button" class="btn btn-sm btn-outline-primary btn-rounded btn-add-new" data-toggle="modal" data-target="#employee-form-modal"><i class="fas fa-plus"></i> Tambah Pegawai</button>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
              
                <!--  Modal employee form -->
                <div class="modal fade" id="employee-form-modal" tabindex="-1" role="dialog"
                    aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="employee-form-label">Large modal</h4>
                                <button type="button" class="close" data-dismiss="modal"
                                    aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                            <form class="container-fluid" action="" method="post" id="employee-form">
                                @csrf
                                <input type="hidden" id="id" name="id">
                                <div class="row mb-3">
                                <div class="col-3">
                                    <label for="name" class="col-form-label">Nama </label>
                                </div>
                                <div class="col-9">
                                    <input type="text" class="form-control" id="name" name="name">
                                </div>
                                </div>
                                <div class="row mb-3">
                                <div class="col-3">
                                <label for="nik" class="col-form-label">NIK</label>
                                </div>
                                <div class="col-9">
                                <input class="form-control" id="nik" name="nik"></input>
                                </div>
                                </div>
                                <div class="row mb-3">
                                <div class="col-3">
                                    <label for="role_id" class="col-form-label">Role User</label>
                                </div>
                                <div class="col-5">
                                    <select class="form-control form-select" aria-label="Pilih Peran Pengguna" id="role_id" name="role_id">
                                    <option value="0" selected>Pilih Peran Pengguna</option>
                                    @foreach($roles as $r)
                                    <option value="{{ $r->id }}">{{ $r->role }}</option>
                                    @endforeach
                                    </select>
                                </div>
                                </div>
                                <div class="row mb-3">
                                <div class="col-3">
                                    <label for="division" class="col-form-label">Divisi</label>
                                </div>
                                <div class="col-4">
                                    <select class="form-control form-select" aria-label="Pilih Divisi Pegawai" id="division" name="division">
                                    <option value="0" selected>Pilih Divisi</option>
                                    @foreach($divisions as $d)
                                    <option value="{{ $d->id }}">{{ $d->name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                                </div>
                                <div class="row mb-3">
                                <div class="col-3">
                                    <label for="title" class="col-form-label">Jabatan</label>
                                </div>
                                <div class="col-4">
                                    <select class="form-control form-select" aria-label="Pilih Jabatan Pegawai" id="title" name="title">
                                    <option value="0" selected>Pilih Jabatan</option>
                                    @foreach($titles as $t)
                                    <option value="{{ $t->id }}">{{ $t->name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                                </div>
                                <div class="row mb-3">
                                <div class="col-3">
                                    <label for="mphone_number" class="col-form-label">No HP</label>
                                </div>
                                <div class="col-9">
                                    <input class="form-control" type="tel" id="mphone_number" name="mphone_number"></input>
                                </div>
                                </div>
                                <div class="row mb-3">
                                <div class="col-3">
                                    <label for="messaging_id" class="col-form-label">Username Telegram</label>
                                </div>
                                <div class="col-9">
                                    <input class="form-control" type="tel" id="messaging_id" name="messaging_id"></input>
                                </div>
                                </div>
                                <div class="row mb-3">
                                <div class="col-3">
                                    <label for="address" class="col-form-label">Alamat</label>
                                </div>
                                <div class="col-9">
                                    <textarea class="form-control" id="address" name="address"></textarea>
                                </div>
                                </div>
                                <div class="row mb-3">
                                <div class="col-3">
                                    <label for="email">Email</label>
                                </div>
                                <div class="col-9">
                                    <input class="input-email form-control form-control-sm" id="email" name="email"></input>
                                </div>
                                </div>
                                <div class="modal-footer">
                                <button type="submit" class="btn btn-primary btn-employee-form">Simpan</button>
                                </div>
                            </form>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <!--  Modal delete data -->
                <div id="delete-row" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none; padding-right: 17px;" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Hapus Data Pegawai</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <h6>Apa Anda yakin untuk menghapus pegawai?</h6>
                                <hr>
                                <p style="font-size:12px;" class="mt-3">Catatan: </p>
                                <ul class="ms-3">
                                <li style="font-size:12px;">Pegawai yang terkait dengan data peminjaman atau dengan jabatan pengemudi hanya bisa dinonaktifkan</li>
                                </ul>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Batal</button>
                                <button type="button" class="btn btn-danger m-delete-btn">Hapus</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>

                <!-- /.tabel-content -->
                <div class="table-responsive">
                    <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                       
                        <div class="row">
                        <div class="col-sm-12">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap dataTable" role="grid" aria-describedby="zero_config_info">
                            <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Nama: Klik untuk mengurutkan data" style="width: 0px;">Nama</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="NIK: Klik untuk mengurutkan data" style="width: 0px;">NIK</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Divisi: Klik untuk mengurutkan data" style="width: 0px;">Divisi</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Jabatan: Klik untuk mengurutkan data" style="width: 0px;">Jabatan</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Email: activate to sort column ascending" style="width: 0px;">Email</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="No HP: Klik untuk mengurutkan data" style="width: 0px;">No HP</th>
                                    <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Aksi: Klik untuk mengurutkan data" style="width: 0px;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>  
                            @foreach($employees as $e)
                                @if($loop->iteration % 2 == 0)									
                                <tr class="odd">
                                    <td class="" tabindex="0">{{ $e->name}}</td>
                                    <td style="">{{ $e->nik }}</td>
                                    <td style="">{{ $e->division->name }}</td>
                                    <td style="">{{ $e->title->name }}</td>
                                    <td style="">{{ $e->email }}</td>
                                    <td style="">{{ $e->mphone_number }}</td>
                                    <td class="table-action">
                                    @if(isset($e->deleted_at))
                                    <a href="#" class="btn btn-secondary btn-sm disabled">Tidak Aktif</a>
                                    @else
                                    <a class="edit-icon" href="#" data-toggle="modal" data-target="#employee-form-modal" 
                                        data-empl='{"id": "{{ $e->id }}", 
                                                    "name": "{{ $e->name }}",
                                                    "nik": "{{ $e->nik }}",
                                                    "division": "{{ $e->division->id}}",
                                                    "title": "{{ $e->title->id }}",
                                                    "role" : "{{$e->user->role_id}}",
                                                    "mphone_number": "{{ $e->mphone_number }}",
                                                    "messaging_id": "{{ $e->messaging_id }}",
                                                    "address": "{{ $e->address }}",
                                                    "email": "{{ $e->email }}"
                                                    }'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 align-middle">
                                        <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                        </svg>
                                    </a>
                                    <a class="delete-icon" href="#" data-toggle="modal" data-target="#delete-row" data-id="{{ $e->id }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash align-middle">
                                        <polyline points="3 6 5 6 21 6"></polyline>
                                        <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                        </path>
                                        </svg>
                                    </a>
                                    
                                    @endif
                                    </td>
                                </tr>
                                @else
                                <tr class="even">
                                <td class="" tabindex="0">{{ $e->name}}</td>
                                    <td style="">{{ $e->nik }}</td>
                                    <td style="">{{ $e->division->name }}</td>
                                    <td style="">{{ $e->title->name }}</td>
                                    <td style="">{{ $e->email }}</td>
                                    <td style="">{{ $e->mphone_number }}</td>
                                    <td class="table-action">
                                    @if(isset($e->deleted_at))
                                    <a href="#" class="btn btn-secondary btn-sm disabled">Tidak Aktif</a>
                                    @else
                                    <a class="edit-icon" href="#" data-toggle="modal" data-target="#employee-form-modal" 
                                        data-empl='{"id": "{{ $e->id }}", 
                                                    "name": "{{ $e->name }}",
                                                    "nik": "{{ $e->nik }}",
                                                    "division": "{{ $e->division->id}}",
                                                    "title": "{{ $e->title->id }}",
                                                    "role" : "{{$e->user->role_id}}",
                                                    "mphone_number": "{{ $e->mphone_number }}",
                                                    "messaging_id": "{{ $e->messaging_id }}",
                                                    "address": "{{ $e->address }}",
                                                    "email": "{{ $e->email }}"
                                                    }'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 align-middle">
                                        <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                        </svg>
                                    </a>
                                    <a class="delete-icon" href="#" data-toggle="modal" data-target="#delete-row" data-id="{{ $e->id }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash align-middle">
                                        <polyline points="3 6 5 6 21 6"></polyline>
                                        <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                        </path>
                                        </svg>
                                    </a>
                                    @endif
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-6">
                                <div class="dataTables_paginate paging_simple_numbers" id="zero_config_paginate">
                                    <ul class="pagination mt-3">
                                    {{ $employees->links() }}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-script')

<script>

$(document).ready(function() {

   
  var table = $('#zero_config').DataTable( {
    paging: false,
    info: false
  });
  
  $(".delete-icon").click(function() {
    var id = $(this).data('id');
    $(".m-delete-btn").click(function() {
      var url = "{{ route('pegawai.delete', ':id') }}";
      url = url.replace(':id', id);
      window.location.href = url;
    });
  });


  $(".btn-add-new").click(function() {

    document.getElementById('employee-form-label').textContent = 'Tambah Pegawai';
    $('#employee-form-modal').on('shown.bs.modal', function() {
        var modal = $(this);
        modal.find('#id').val("");
        modal.find('#name').val("");
        modal.find('#nik').val("");
        modal.find('#division').val(0);
        modal.find('#title').val(0);
        modal.find('#role_id').val(0);
        modal.find('#mphone_number').val("");
        modal.find('#messaging_id').val("");
        modal.find('#address').val("");
        modal.find('#email').val("");
        modal.find('#employee-form').attr("action", "{{ route('pegawai.save') }}");
    });
  });


  $(".edit-icon").click(function() {
    var empl = $(this).data('empl');
    console.log(empl);
    document.getElementById('employee-form-label').textContent = 'Ubah Data Pegawai';
     $('#employee-form-modal').on('shown.bs.modal', function() {
        var modal = $(this);
        modal.find('#id').val(empl.id);
        modal.find('#name').val(empl.name);
        modal.find('#nik').val(empl.nik);
        modal.find('#division').val(empl.division);
        modal.find('#title').val(empl.title);
        modal.find('#role_id').val(empl.role);
        modal.find('#mphone_number').val(empl.mphone_number);
        modal.find('#messaging_id').val(empl.messaging_id);
        modal.find('#address').val(empl.address);
        modal.find('#email').val(empl.email);

        modal.find('#employee-form').attr("action", "{{ route('pegawai.update') }}");
     });
  });
});

</script>

@endsection
