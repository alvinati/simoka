<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('request_id')->constrained('requests');
            $table->foreignId('status_id')->constrained('booking_status');
            $table->string('book_number', 100);
            $table->decimal('total_cost', $precision=8, $scale=2)->nullable();
            $table->decimal('toll_cost', $precision=8, $scale=2)->nullable();
            $table->decimal('fuel_cost', $precision=8, $scale=2)->nullable();
            $table->datetime('return_date')->nullable();
            $table->datetime('exit_date');
            $table->timestamps();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
