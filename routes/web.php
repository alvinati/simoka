<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\RequestController;
use App\Http\Controllers\AdminDashboardController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\DriverController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\OrganizationController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\TelegramController;
use App\Http\Controllers\BookingController;

use App\Http\Controllers\RequestorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', IndexController::class);


//Authentication Region
Route::get('login', AuthController::class);
Route::post('login', [AuthController::class, 'doLogin'])->name('login');
Route::get('logout', [AuthController::class, 'logout']);
Route::get('/ganti-password/{token}', [AuthController::class, 'showChangePassword'])->name('password.change');
Route::post('/ganti-password', [AuthController::class, 'changePassword']);

Route::get('lupa-password', function() {
    return view('auth.forgot-password');
})->middleware('guest')->name('password.request');
Route::post('/lupa-password', [AuthController::class, 'reqResetPassword']);

Route::post('/'.env('TELEGRAM_BOT_TOKEN').'/webhook', [TelegramController::class, 'processIncoming']);

Route::middleware(['auth', 'role:admin'])->group(function() {

    Route::get('/admin', AdminDashboardController::class)->name('admin');
   
    Route::get('/pengajuan', RequestController::class);
    Route::get('/pengajuan/{id}/{action}',[RequestController::class, 'validation']);
    Route::post('/pengajuan/save',[RequestController::class, 'saveRequestChange'])->name('pengajuan.save');
    Route::post('/pengajuan/decline', [RequestController::class, 'declineRequest'])->name('pengajuan.decline');
   
    Route::get('/peminjaman', BookingController::class);
    Route::get('/peminjaman/{id}', [BookingController::class, 'detailBooking']);
    Route::post('/peminjaman/finish', [BookingController::class, 'finishBooking']);
    Route::post('/peminjaman/cancel', [BookingController::class, 'cancelBooking']);

    Route::get('/master/pegawai', EmployeeController::class);
    Route::get('/master/pegawai/delete/{id}', [EmployeeController::class, 'delete'])->name('pegawai.delete');
    Route::post('/master/pegawai/save', [EmployeeController::class, 'save'])->name('pegawai.save');
    Route::post('/master/pegawai/update', [EmployeeController::class, 'update'])->name('pegawai.update');

    Route::get('/master/kendaraan', VehicleController::class)->name('kendaraan');
    Route::get('/master/kendaraan/delete/{id}', [VehicleController::class, 'delete'])->name('kendaraan.delete');
    Route::get('/master/kendaraan/search', [VehicleController::class, 'search'])->name('kendaraan.search');
    Route::post('/master/kendaraan/save', [VehicleController::class, 'save'])->name('kendaraan.save');
    Route::post('/master/kendaraan/update', [VehicleController::class, 'update'])->name('kendaraan.update');

    Route::get('/master/pengemudi', DriverController::class);
    Route::post('/master/pengemudi/update', [DriverController::class, 'update'])->name('pengemudi.update');

    Route::get('/master/divisi-jabatan', OrganizationController::class);
    Route::post('/master/divisi-jabatan/simpan', [OrganizationController::class, 'saveChanges']);
    Route::get('/master/divisi-jabatan/hapus-divisi/{id}', [OrganizationController::class, 'deleteDivision'])->name('division.delete');
    Route::get('/master/divisi-jabatan/hapus-jabatan/{id}', [OrganizationController::class, 'deleteTitle'])->name('title.delete');

    Route::get('/profil', AccountController::class);
    Route::post('/profil/update', [AccountController::class, 'updateProfile']);
    Route::post('/profil/password', [AccountController::class, 'updatePassword']);

});

Route::middleware(['auth', 'role:admin,reviewer'])->group(function() {
    
    Route::get('/laporan', ReportController::class);
    Route::get('/laporan/{id}', [ReportController::class, 'reportDetail']);
    Route::post('/laporan', [ReportController::class, 'showFiltered']);
    Route::post('/laporan/export_excel', [ReportController::class, 'exportExcel'])->name('laporan.export');

});

Route::middleware(['auth'])->group(function(){
    Route::get('/dashboard', RequestorController::class);
    Route::get('/riwayat-pengajuan/detail/{id}', [RequestorController::class, 'requestDetail']);
    Route::get('/form-pengajuan', [RequestorController::class, 'requestForm'])->name('form.request');
    Route::post('/submit-form', [RequestorController::class, 'submitForm']);
    Route::get('/riwayat-pengajuan', [RequestorController::class, 'showRequestLists']);
    Route::get('/riwayat-pengajuan/detail-peminjaman/{id}', [RequestorController::class, 'bookingDetail']);
    Route::get('/myprofile', [RequestorController::class, 'profile']);
    Route::post('/update-profil', [RequestorController::class, 'updateMyProfile']);
});
